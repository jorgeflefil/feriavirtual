from django.conf import settings
from django.contrib import messages
from django.contrib.auth.decorators import user_passes_test, login_required
from django.core.mail import send_mail
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import render, redirect
from django.template.loader import get_template
from django.urls import reverse
from django.utils.decorators import method_decorator
from django.views.generic import ListView
from django.views.generic.edit import FormView
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import viewsets, response, status, parsers, decorators
from rest_framework.decorators import api_view
from rest_framework.parsers import FileUploadParser, MultiPartParser, FormParser
from rest_framework.response import Response
from xhtml2pdf import pisa
from django.db.models import Q, Sum

from ..forms import *
from ..serializer import *


# ====== RENDER PDF ======
def venta_externa_render_pdf_view(request, *args, **kwargs):
    pk = kwargs.get('pk')
    proceso_venta = ProcesoVenta.objects.get(pk=pk)
    productos_requeridos = ProductoRequerido.objects.filter(solicitud_compra=proceso_venta.solicitud_compra)

    proceso_postulacion = ProcesoPostulacion.objects.get(proceso_venta=proceso_venta)

    # AGREGAR A FILTRO -> SOLO CON ESTADO GANADOR
    postulaciones_productores = PostulacionProductor.objects.filter(proceso_postulacion=proceso_postulacion)

    envios_productores = EnvioProductoProductor.objects.filter(postulacion_productor__in=postulaciones_productores)

    template_path = 'pdf/informe_venta_externa.html'
    context = {'proceso_venta': proceso_venta, 'productos_requeridos': productos_requeridos,
               'postulaciones_productores': postulaciones_productores, 'envios_productores': envios_productores}

    # Create a Django response object, and specify content_type as pdf
    response = HttpResponse(content_type='application/pdf')

    # if download:
    response['Content-Disposition'] = 'attachment; filename="report.pdf"'
    # if display:
    response['Content-Disposition'] = 'filename="report.pdf"'
    # find the template and render it.
    template = get_template(template_path)
    html = template.render(context)

    # create a pdf
    pisa_status = pisa.CreatePDF(
        html, dest=response)
    # if error then show some funy view
    if pisa_status.err:
        return HttpResponse('We had some errors <pre>' + html + '</pre>')
    return response


# ====== VIEWSETS ======

class ListaSubastaTransportistaViewSet(viewsets.ModelViewSet):
    queryset = Subasta.objects.all()
    serializer_class = ListaSubastaSerializer


class ListaProductoViewSet(viewsets.ModelViewSet):
    queryset = Producto.objects.all()
    serializer_class = ListaProductoSerializer
    pagination_class = None


class ProductoViewSet(viewsets.ModelViewSet):
    queryset = Producto.objects.all()
    serializer_class = ProductoSerializer
    parser_classes = (MultiPartParser, FileUploadParser, FormParser)

    @decorators.action(
        detail=True,
        methods=['PUT'],
        serializer_class=ProductoImgSerializer,
        parser_classes=[parsers.MultiPartParser],
    )
    def pic(self, request):
        obj = self.get_object()
        serializer = self.serializer_class(obj, data=request.data,
                                           partial=True)
        if serializer.is_valid():
            serializer.save()
            return response.Response(serializer.data)
        return response.Response(serializer.errors,
                                 status.HTTP_400_BAD_REQUEST)


class ProcesoProductorViewSet(viewsets.ModelViewSet):
    queryset = ProcesoVenta.objects.all()
    serializer_class = ProcesoVentaSerializer


class ProcesoAdminViewSet(viewsets.ModelViewSet):
    queryset = ProcesoVenta.objects.all()
    serializer_class = ProcesoVentaSerializer


class ProcesoVentaAdminViewSet(viewsets.ModelViewSet):
    queryset = ProcesoVenta.objects.all()
    serializer_class = ProcesoVentaSerializer

    def create(self, request, pk=None):
        # Actualizar estado de solicitud de compra
        solicitud_compra = SolicitudDeCompra.objects.get(pk=request.POST["solicitud_compra"])
        solicitud_compra.estado_solicitud = "Solicitud aprobada"
        solicitud_compra.save()

        # Crear proceso de venta
        proceso_venta = ProcesoVenta()
        proceso_venta.estado_proceso = "En proceso de postulación de productores"
        proceso_venta.solicitud_compra = solicitud_compra
        proceso_venta.pedido = None
        proceso_venta.save()

        # Crear proceso de postulacion de productores
        proceso_postulacion = ProcesoPostulacion()
        proceso_postulacion.estado_proceso = "En proceso de postulación de productores"
        proceso_postulacion.proceso_venta = proceso_venta
        proceso_postulacion.save()

        return Response(data=proceso_venta.pk)


class ProcesoVentaProductorViewSet(viewsets.ModelViewSet):
    queryset = ProcesoVenta.objects.all().filter(estado_proceso="En proceso de postulación de productores")
    serializer_class = ProcesoVentaSerializer


class ProcesoPostulacionProductorViewSet(viewsets.ModelViewSet):
    queryset = ProcesoPostulacion.objects.all()
    serializer_class = ProcesoPostulacionSerializer


class DetalleProcesoPostulacionProductorViewSet(viewsets.ModelViewSet):
    queryset = ProcesoPostulacion.objects.all().filter(estado_proceso="En proceso de postulación de productores")
    serializer_class = DetalleProcesoPostulacionSerializer


class DetalleProcesoPostulacionAdminViewSet(viewsets.ModelViewSet):
    queryset = ProcesoPostulacion.objects.all()
    serializer_class = DetalleProcesoPostulacionSerializer


class MisPostulacionesProductorViewSet(viewsets.ModelViewSet):
    queryset = PostulacionProductor.objects.all()
    serializer_class = PostulacionProductorSerializer

    def get_queryset(self):
        lista_procesos_venta = PostulacionProductor.objects.filter(
            productor=Productor.objects.get(pk=self.request.user))
        return lista_procesos_venta


class ClienteExternoMisProcesosVentaProductorViewSet(viewsets.ModelViewSet):
    queryset = ProcesoPostulacion.objects.all()
    serializer_class = ProcesoPostulacionSerializer

    def get_queryset(self):
        lista_procesos_venta = ProcesoPostulacion.objects.filter(proceso_venta__solicitud_compra__cliente_externo__user=self.request.user)
        return lista_procesos_venta


class PostulacionProductorViewSet(viewsets.ModelViewSet):
    queryset = PostulacionProductor.objects.all()
    serializer_class = PostulacionProductorSerializer
    filter_backends = [DjangoFilterBackend]
    filterset_fields = ['proceso_postulacion', 'productor']

    def create(self, request, pk=None):
        # ASIGNAR EL PRODUCTO AL PRODUCTOR
        postulacion_productor = PostulacionProductor()

        postulacion_productor.proceso_postulacion = ProcesoPostulacion.objects.get(
            pk=request.POST["proceso_postulacion"])
        postulacion_productor.productor = Productor.objects.get(pk=request.user)
        postulacion_productor.estado_postulacion = "Postulando"
        postulacion_productor.save()

        return Response(data=postulacion_productor.pk)


class SubastasTransportistaViewSet(viewsets.ModelViewSet):
    queryset = Subasta.objects.all().filter(estado_subasta="En curso")
    serializer_class = ListaSubastaSerializer


class SubastasGanadasTransportistaViewSet(viewsets.ModelViewSet):
    queryset = Subasta.objects.filter()
    serializer_class = ListaSubastaSerializer
    pagination_class = None

    def get_queryset(self):
        lista_subastas_ganadas = Subasta.objects.all().filter(
            transportista=Transportista.objects.get(user_id=self.request.user))
        return lista_subastas_ganadas


class SubastasFinalizadasViewSet(viewsets.ModelViewSet):
    queryset = Subasta.objects.filter(estado_subasta__in = ('Subasta finalizada','Subasta con transportista'))
    serializer_class = ListaSubastaSerializer


class SubastasAdminViewSet(viewsets.ModelViewSet):
    queryset = Subasta.objects.all().filter()
    serializer_class = ListaSubastaSerializer

    def create(self, request, pk=None):
        subasta = Subasta()
        subasta.id_subasta = self.request.POST["id_subasta"]
        subasta.fecha_termino = self.request.POST["fecha_termino"]
        subasta.direccion_retiro = self.request.POST["direccion_retiro"]
        subasta.direccion_entrega = self.request.POST["direccion_entrega"]
        subasta.estado_subasta = self.request.POST["estado_subasta"]
        subasta.proceso = ProcesoVenta.objects.get(pk=self.request.POST["codigo_proceso"])

        subasta.save()

        return Response(data=subasta.pk)


class MejorOfertaTransportistaViewSet(viewsets.ModelViewSet):
    queryset = PostulacionTransportista.objects.all().filter(estado_postulacion="Mejor oferta")
    serializer_class = MejorPujaSerializer
    pagination_class = None


class PujasViewSet(viewsets.ModelViewSet):
    queryset = PostulacionTransportista.objects.filter()
    serializer_class = ListaPujaAdminSerializer
    pagination_class = None


    def create(self, request, pk=None):
        objs = PostulacionTransportista.objects.filter(
            id_subasta=Subasta.objects.get(id_subasta=self.request.POST["id_subasta"]))
        objs.update(estado_postulacion="Pendiente")

        puja = PostulacionTransportista()
        puja.id_subasta = Subasta.objects.get(id_subasta=self.request.POST["id_subasta"])
        puja.id_transportista = Transportista.objects.get(user_id=self.request.POST["id_transportista"])
        puja.valor_puja = self.request.POST["valor_puja"]
        puja.estado_postulacion = self.request.POST["estado_postulacion"]

        puja.save()

        return Response(data=self.request.POST["id_subasta"])


class PujasAdminViewSet(viewsets.ModelViewSet):
    queryset = PostulacionTransportista.objects.filter()
    serializer_class = ListaPujaAdminSerializer
    filter_backends = [DjangoFilterBackend]
    filterset_fields = ['id_subasta']
    pagination_class = None



class PujasTransportistaViewSet(viewsets.ModelViewSet):
    queryset = PostulacionTransportista.objects.filter()
    serializer_class = ListaPujaAdminSerializer

    def get_queryset(self):
        lista_pujas_transportista = PostulacionTransportista.objects.all().filter(
            id_transportista=Transportista.objects.get(user_id=self.request.user))
        return lista_pujas_transportista


# Busqueda mantenedor admin
class UserClienteExternoViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all().filter(user_type=1)
    serializer_class = UserSerializer


class UserClienteInternoViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all().filter(user_type=2)
    serializer_class = UserSerializer


class UserTransportistaViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all().filter(user_type=3)
    serializer_class = UserSerializer


class UserConsultorViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all().filter(user_type=4)
    serializer_class = UserSerializer


class UserProductorViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all().filter(user_type=5)
    serializer_class = UserSerializer


class ProductorViewSet(viewsets.ModelViewSet):
    queryset = Productor.objects.all().filter(user__user_type=5)
    serializer_class = ProductorSerializer


class GetAllContratoProductor(viewsets.ModelViewSet):
    queryset = ContratoProductor.objects.all()
    serializer_class = ContratoProductorSerializer
    filter_backends = [DjangoFilterBackend]
    filterset_fields = ['productor']
    pagination_class = None


class GetAllProducts(viewsets.ModelViewSet):
    queryset = ProductorProducto.objects.all()
    serializer_class = ProductorProductoSerializer


class GetAllProductsHighQuality(viewsets.ModelViewSet):
    queryset = ProductorProducto.objects.filter(calidad=1).values("producto__nombre_producto").annotate(Sum('cantidad'))
    serializer_class = ProductorProductoCalidadSerializer
    pagination_class = None


class GetAllProductsMediumQuality(viewsets.ModelViewSet):
    queryset = ProductorProducto.objects.filter(calidad=2).values("producto__nombre_producto").annotate(Sum('cantidad'))
    serializer_class = ProductorProductoCalidadSerializer
    pagination_class = None


class GetAllProductsLowQuality(viewsets.ModelViewSet):
    queryset = ProductorProducto.objects.filter(calidad=3).values("producto__nombre_producto").annotate(Sum('cantidad'))
    serializer_class = ProductorProductoCalidadSerializer
    pagination_class = None


class GetProductosProductor(viewsets.ModelViewSet):
    queryset = ProductorProducto.objects.all()
    serializer_class = ProductorProductoSerializer

    def get_queryset(self):
        productos_productor = ProductorProducto.objects.all().filter(productor__user=self.request.user.id)
        return productos_productor


class Shopping(viewsets.ModelViewSet):
    serializer_class = CartSerializer

    def get_queryset(self):
        user_cart = Cart.objects.filter(user_id_id=User.objects.get(id=self.request.user.id))
        return user_cart


class GetSaleProduct(viewsets.ModelViewSet):
    queryset = ProductorProducto.objects.filter(interna=True)


class UserProductorProductoViewSet(viewsets.ModelViewSet):
    queryset = ProductorProducto.objects.all()
    serializer_class = ProductorProductoSerializer
    filter_backends = [DjangoFilterBackend]
    filterset_fields = ['productor']
    pagination_class = None

    def get_queryset(self):
        lista_productos_productor = ProductorProducto.objects.all().filter(productor__user=self.request.user.id)
        return lista_productos_productor

    def create(self, request, pk=None):
        # ASIGNAR EL PRODUCTO AL PRODUCTOR
        productor_producto = ProductorProducto()
        productor_producto.producto = Producto.objects.get(pk=self.request.POST["producto_productor"])
        productor_producto.precio_unitario = self.request.POST["precio_unitario_nuevo_producto"]
        productor_producto.cantidad = self.request.POST["cantidad_nuevo_producto"]
        productor_producto.calidad = self.request.POST.get("calidad_nuevo_producto")

        ProductorProducto.objects.create(productor=Productor.objects.get(user=request.user),
                                         producto=productor_producto.producto,
                                         precio_unitario=productor_producto.precio_unitario,
                                         cantidad=productor_producto.cantidad, calidad=productor_producto.calidad)

        return Response(data=productor_producto.pk)


class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all().exclude(is_staff=True)
    serializer_class = UserSerializer

    def create(self, request, pk=None):
        user = User()
        user.first_name = self.request.POST["nombre"]
        user.last_name = self.request.POST["apellidos"]
        user.email = self.request.POST["email"]
        user.user_type = self.request.POST["user_type"]
        user.set_password(self.request.POST["password"])
        user.username = self.request.POST["email"]

        user.save()

        return Response(data=user)


class SubastasListView(ListView):
    model = Subasta
    template_name = 'subastas_transportista.html'
    context_object_name = 'lista_subastas_transportista'

    def get_queryset(self):
        lista_subastas_transportista = Subasta.objects.filter()
        return lista_subastas_transportista

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(SubastasListView, self).dispatch(*args, **kwargs)


class SubastasGanadasListView(ListView):
    model = Subasta
    template_name = 'subastas_ganadas_transportista.html'
    context_object_name = 'lista_subastas_ganadas_transportista'

    def get_queryset(self):
        lista_subastas_ganadas_transportista = Subasta.objects.filter()
        return lista_subastas_ganadas_transportista

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(SubastasGanadasListView, self).dispatch(*args, **kwargs)


class ProcesoAdminListView(ListView):
    model = ProcesoVenta
    template_name = 'procesos_admin.html'
    context_object_name = 'lista_procesos_admin'

    def get_queryset(self):
        lista_procesos_admin = ProcesoVenta.objects.filter()
        return lista_procesos_admin

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(ProcesoAdminListView, self).dispatch(*args, **kwargs)


@user_passes_test(lambda u: u.is_superuser)
def reportes_admin(request):
    if request.user.is_authenticated:
        return render(request, 'reportes_admin.html', {})
    else:
        return render(request, 'login.html', {})


class SubastasAdminListView(ListView):
    model = Subasta
    template_name = 'subastas_admin.html'
    context_object_name = 'lista_subastas_admin'

    def get_queryset(self):
        lista_subastas_admin = Subasta.objects.filter()
        return lista_subastas_admin

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(SubastasAdminListView, self).dispatch(*args, **kwargs)


class ProcesoProductorListView(ListView):
    model = ProcesoVenta
    template_name = 'postular.html'
    context_object_name = 'lista_procesos_productor'

    def get_queryset(self):
        lista_procesos_productor = ProcesoVenta.objects.filter()
        return lista_procesos_productor

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(ProcesoProductorListView, self).dispatch(*args, **kwargs)


class UsuarioListView(ListView):
    model = User
    template_name = 'mantenedor.html'
    context_object_name = 'lista_usuarios'

    def get_queryset(self):
        lista_usuarios = User.objects.filter()
        return lista_usuarios

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(UsuarioListView, self).dispatch(*args, **kwargs)


class AllSolicitudDeCompraViewSet(viewsets.ModelViewSet):
    queryset = SolicitudDeCompra.objects.all()
    serializer_class = SolicitudDeCompraSerializer


class SolicitudDeCompraViewSet(viewsets.ModelViewSet):
    queryset = SolicitudDeCompra.objects.all()
    serializer_class = SolicitudDeCompraSerializer

    def get_queryset(self):
        lista_solicitud_compra = SolicitudDeCompra.objects.all().filter(cliente_externo=self.request.user.id)
        return lista_solicitud_compra


class DetalleSolicitudDeCompraViewSet(viewsets.ModelViewSet):
    queryset = SolicitudDeCompra.objects.all()
    serializer_class = DetalleSolicitudDeCompraSerializer


class ProductoRequeridoViewSet(viewsets.ModelViewSet):
    queryset = ProductoRequerido.objects.all()
    serializer_class = ProductoRequeridoSerializer
    filter_backends = [DjangoFilterBackend]
    filterset_fields = ['solicitud_compra']

    def create(self, request, *args, **kwargs):
        cantidad_productos_requeridos = request.POST["cantidad_productos"]
        print("Cantidad de productos ingresados: " + cantidad_productos_requeridos)
        cantidad_productos_requeridos = int(cantidad_productos_requeridos)

        productos = request.POST.getlist('producto')
        calidad = request.POST.getlist('calidad')
        cantidad = request.POST.getlist('cantidad')
        solicitud_compra = SolicitudDeCompra.objects.get(pk=request.POST["solicitud_compra"])

        for i, item in enumerate(productos, start=0):
            print("-- Ingresando producto " + str(i) + " --")

            producto = Producto.objects.get(pk=productos[i])
            pruducto_requerido = ProductoRequerido()

            pruducto_requerido.solicitud_compra = solicitud_compra
            pruducto_requerido.producto = producto
            pruducto_requerido.calidad = calidad[i]
            pruducto_requerido.cantidad = cantidad[i]
            pruducto_requerido.save()
        return Response("Productos ingresados correctamente", status=status.HTTP_201_CREATED)


class ProductorValidarProductoRequeridoViewSet(viewsets.ModelViewSet):
    queryset = ProductoRequerido.objects.all()
    serializer_class = ProductoRequeridoSerializer
    filter_backends = [DjangoFilterBackend]
    filterset_fields = ['solicitud_compra']
    pagination_class = None


class ViewSet(viewsets.ModelViewSet):
    queryset = ProductoRequerido.objects.all()
    serializer_class = ProductoRequeridoSerializer
    filter_backends = [DjangoFilterBackend]
    filterset_fields = ['solicitud_compra']
    pagination_class = None


def venta_interna(request):
    if request.user.is_authenticated:
        return render(request, 'venta_interna.html', {})
    else:
        return render(request, 'login.html', {})


def content(request):

    return render(request, 'content.html', {})


def product_list(request):
    products = ProductorProducto.objects.filter(interna=True, cantidad__gte=1)
    return render(request, 'plp.html', {'products': products})


def basket(request):
    try:
        order = Order.objects.get(user_id=request.user.id, status='Unpaid')
        cart = Cart.objects.filter(user_id=request.user.id, order_id=order.order_id)
    except:
        cart = ''
    return render(request, 'cart.html', {'cart': cart})


def add_to_cart(request):
    if request.user.id is None:
        return HttpResponseRedirect(reverse('sign_up'))
    order = Order.objects.filter(user_id=request.user.id, status='Unpaid')
    if order.count() == 0:
        order = Order()
        order.user_id = User.objects.get(id=request.user.id)
        order.address_id = UserAddress.objects.get(address_id=1)
        order.payment_id = Payment.objects.get(payment_id=1)
        order.save()

    order = Order.objects.get(user_id=request.user.id, status='Unpaid')
    cart = Cart.objects.filter(order_id=order.order_id)
    if cart.count() == 0:
        cart = Cart()
        cart.user_id = User.objects.get(id=request.user.id)
        cart.product_id = Producto.objects.get(id=request.POST.get('product'))
        cart.productor_id = ProductorProducto.objects.get(id=request.POST.get('productor'))
        cart.order_id = Order.objects.get(user_id=request.user.id, status='Unpaid')
        cart.quantity = request.POST.get('quantity')
        cart.price = request.POST.get('price')
        cart.unidad = request.POST.get('unidad')
        cart.save()
    else:
        for c in cart:
            if c.product_id == Producto.objects.get(id=request.POST.get('product')):
                c.quantity += int(request.POST.get('quantity'))
                c.price = int(request.POST.get('price')) * c.quantity
                c.save()
            else:
                cart = Cart()
                cart.user_id = User.objects.get(id=request.user.id)
                cart.product_id = Producto.objects.get(id=request.POST.get('product'))
                cart.productor_id = ProductorProducto.objects.get(id=request.POST.get('productor'))
                cart.order_id = Order.objects.get(user_id=request.user.id, status='Unpaid')
                cart.quantity = request.POST.get('quantity')
                cart.price = request.POST.get('price')
                cart.unidad = request.POST.get('unidad')
                cart.save()
    messages.success(request, 'Se ha agregado el producto al carro.')
    return HttpResponseRedirect(reverse('products'))


def removecart(request, id):
    cart = Cart.objects.filter(id=id)
    cart.delete()
    messages.success(request, 'Se ha eliminado el producto del carro.')
    return HttpResponseRedirect(reverse('cart'))


def address(request):
    user_address = UserAddress()
    user_address.user_id = User.objects.get(id=request.user.id)
    user_address.street = request.POST.get("street")
    user_address.number = request.POST.get("number")
    user_address.region = request.POST.get("region")
    user_address.comuna = request.POST.get("comuna")
    user_address.save()
    messages.success(request, 'Se ha agregado una nueva dirección.')
    return HttpResponseRedirect(reverse('shipping'))


def new_address(request):
    user_address = UserAddress()
    user_address.user_id = User.objects.get(id=request.user.id)
    user_address.street = request.POST.get("street")
    user_address.number = request.POST.get("number")
    user_address.region = request.POST.get("region")
    user_address.comuna = request.POST.get("comuna")
    user_address.save()
    messages.success(request, 'Se ha agregado una nueva dirección.')
    return HttpResponseRedirect(reverse('my_address'))


def delete_address(request):
    try:
        user_address = UserAddress.objects.get(address_id=request.POST.get('address'))
        user_address.enabled = False
        user_address.save()
    except:
        None
    messages.success(request, 'Se ha eliminado la dirección.')
    return HttpResponseRedirect(reverse('my_address'))


def my_orders_detail(request):
    try:
        order = Order.objects.get(user_id=request.user.id, order_id=request.POST.get('order_id'))
    except:
        order = Order.objects.filter(user_id=request.user.id).reverse().first()
    products = Cart.objects.filter(order_id=order.order_id)
    return render(request, 'my_orders_detail.html', {'order': order, 'products': products})


def update_order_address(request):
    if request.POST.get('delete'):
        try:
            user_address = UserAddress.objects.get(address_id=request.POST.get('address'))
            user_address.enabled = False
            user_address.save()
            messages.success(request, 'Se ha eliminado la dirección.')
        except:
            messages.error(request, 'Ha ocurrido un error, por favor vuelve a intentarlo.')
            return HttpResponseRedirect(reverse('cart'))
        return HttpResponseRedirect(reverse('shipping'))
    else:
        try:
            order = Order.objects.get(user_id=request.user.id, status='Unpaid')
            order.address_id = UserAddress.objects.get(address_id=request.POST.get("address"))
            order.save()
            return HttpResponseRedirect(reverse('payment'))
        except:
            messages.error(request, 'Ha ocurrido un error, por favor vuelve a intentarlo.')
            return HttpResponseRedirect(reverse('cart'))


def update_order_payment(request):
    try:
        order = Order.objects.get(user_id=request.user.id, status='Unpaid')
        if order.address_id_id == 1:
            return HttpResponseRedirect(reverse('shipping'))
        else:
            order.payment_id = Payment.objects.get(payment_id=request.POST.get("payment"))
            order.save()
            return HttpResponseRedirect(reverse('gateway'))
    except:
        return HttpResponseRedirect(reverse('cart'))


def update_order_status(request):
    try:
        order = Order.objects.get(user_id=request.user.id, status='Unpaid')
        cart = Cart.objects.filter(order_id=order.order_id)
        for c in cart:
            pp = ProductorProducto.objects.get(productor_id=c.productor_id.productor_id, producto_id=c.product_id)
            pp.cantidad -= c.quantity
            pp.save()
        order.status = 'Paid'
        order.save()
        return HttpResponseRedirect(reverse('confirmation'))
    except:
        messages.error(request, 'Ha ocurrido un error, por favor vuelve a intentarlo.')
        return HttpResponseRedirect(reverse('cart'))


@api_view(['GET'])
def get_product_price(request):
    if request.method == 'GET':
        return Response(status=status.HTTP_201_CREATED, data=1990)


class CrearSubastaView(FormView):
    template_name = "subastas_admin.html"
    form_class = CrearSubastaForm

    def form_valid(self, form):
        subasta = form.save(commit=False)
        subasta.save()

        return redirect('crear_subasta')


@api_view(['PUT'])
def api_actualizar_productor_producto(request, id_relacion):
    try:
        productor_producto = ProductorProducto.objects.get(pk=id_relacion)
    except ProductorProducto.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    if request.method == 'PUT':
        serializer = ModifProductorProductoSerializer(productor_producto, data=request.data)
        data = {}
        if serializer.is_valid():
            serializer.save()
            data["success"] = "Update successful"
            return Response(data=data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(['PUT'])
def update_recibir(request, id_proceso):
    try:
        ProcesoVenta.objects.filter(pk=id_proceso).update(estado_proceso="Pedido entregado")
        return Response(status=status.HTTP_200_OK)
    except ProcesoVenta.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)


@api_view(['PUT'])
def update_interna(request, id_relacion):
    try:
        internal = ProductorProducto.objects.get(pk=id_relacion).interna
        ProductorProducto.objects.filter(pk=id_relacion).update(interna=not internal)
        return Response(status=status.HTTP_200_OK)
    except ProductorProducto.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)


@api_view(['PUT'])
def update_proceso_transportista(request, id_proceso, transportista):
    try:
        ProcesoVenta.objects.filter(pk=id_proceso).update(transportista=transportista, estado_proceso="En proceso de despacho")
        subject = "Haz sido seleccionado como ganador en la subasta"

        message = "Saludos, Te informamos que haz sido seleccionado como ganador en la subasta en cual concursaste," \
                  " para ver los detalles de la dirección de entrega/retiro y el detalle del despacho, " \
                  "ingresa a 'Subastas ganadas' en nuestra página web."

        email_from = settings.EMAIL_HOST_USER

        recipient_list = User.objects.get(pk=transportista)
        recipient_list = [recipient_list.email, ]

        send_mail(subject, message, email_from, recipient_list, fail_silently=False)
        return Response(status=status.HTTP_200_OK)

    except ProcesoVenta.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

@api_view(['PUT'])
def update_subasta_asignar(request, id_subasta, transportista):
    try:
        Subasta.objects.filter(pk=id_subasta).update(estado_subasta="Subasta con transportista", transportista = transportista)

        return Response(status=status.HTTP_200_OK)
    except Subasta.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)


@api_view(['PUT'])
def update_subasta_estado(request, id_subasta):
    try:
        Subasta.objects.filter(pk=id_subasta).update(estado_subasta="Subasta finalizada")

        return Response(status=status.HTTP_200_OK)
    except Subasta.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

@api_view(['PUT'])
def update_proceso(request, id_proceso):
    try:
        ProcesoPostulacion.objects.filter(pk=id_proceso).update(estado_proceso="En proceso de despacho")

        return Response(status=status.HTTP_200_OK)
    except ProcesoPostulacion.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

@api_view(['PUT'])
def update_proceso_estado_subasta(request, id_proceso):
    try:
        ProcesoVenta.objects.filter(pk=id_proceso).update(estado_proceso="Esperando asignacion de transportista")

        return Response(status=status.HTTP_200_OK)
    except ProcesoVenta.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)



@api_view(['PUT'])
def update_interna_unidad(request, id_relacion):
    try:
        unidad = ProductorProducto.objects.get(pk=id_relacion).unidad
        ProductorProducto.objects.filter(pk=id_relacion).update(unidad='C/U' if unidad == 'KG' else 'KG')
        return Response(status=status.HTTP_200_OK)
    except ProductorProducto.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)


@api_view(['PUT'])
def update_contrato_activar(request, id_contrato):
    try:
        contrato_productor = ContratoProductor.objects.get(pk=id_contrato)
        contrato_productor.fecha_inicio = datetime.today()
        contrato_productor.fecha_termino = datetime.today() + timedelta(days=365)
        contrato_productor.estado = "Vigente"
        contrato_productor.save()

        return Response(status=status.HTTP_200_OK)
    except ProductorProducto.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)


@api_view(['PUT'])
def update_contrato_anular(request, id_contrato):
    try:
        contrato_productor = ContratoProductor.objects.get(pk=id_contrato)
        # contrato_productor.fecha_inicio = datetime.today()
        # contrato_productor.fecha_termino = datetime.today() + timedelta(days=365)
        contrato_productor.estado = "Expirado"
        contrato_productor.save()

        return Response(status=status.HTTP_200_OK)
    except ProductorProducto.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)


@api_view(['POST'])
def api_crear_solicitud_compra(request):
    cliente_externo = ClienteExterno.objects.get(user=request.user)

    solicitud_compra = SolicitudDeCompra(cliente_externo=cliente_externo)

    if request.method == 'POST':
        serializer = SolicitudDeCompraSerializer(solicitud_compra, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


# ====== VIEWS ======
def index(request):
    return render(request, 'index.html', {})


def login(request):
    return render(request, 'login.html', {})


def contactar_transportista(request):
    if request.method == "POST":
        subject = request.POST.get("asunto", True)

        message = request.POST.get("mensaje", True)

        email_from = settings.EMAIL_HOST_USER

        recipient_list = [request.POST.get("email", True)]

        send_mail(subject, message, email_from, recipient_list, fail_silently=True)

        return render(request, "subastas_admin_contacto.html", {})

    return render(request, "subastas_admin_contacto.html", {})


def subastas_transportista(request):
    if request.user.is_authenticated and request.user.user_type == 3:
        return render(request, 'subastas_transportista.html', {})
    else:
        return render(request, 'login.html', {})


def postular(request):
    if request.user.is_authenticated and request.user.user_type == 5:
        return render(request, 'postular.html', {})
    else:
        return render(request, 'login.html', {})


def mis_productos(request):
    if request.user.is_authenticated and request.user.user_type == 5:
        return render(request, 'productor_mis_productos.html', {})
    else:
        return render(request, 'login.html', {})


def productor_mis_procesos_venta(request):
    if request.user.is_authenticated and request.user.user_type == 5:
        return render(request, 'productor_procesos_venta.html', {})
    else:
        return render(request, 'login.html', {})

def cliente_externo_mis_procesos_venta(request):
    if request.user.is_authenticated and request.user.user_type == 1:
        return render(request, 'cliente_externo_proceso_venta.html', {})
    else:
        return render(request, 'login.html', {})


def shipping(request):
    address = UserAddress.objects.filter(user_id=request.user.id, enabled=True)
    return render(request, 'shipping.html', {'address': address})


def payment(request):
    return render(request, 'payment.html')


def gateway(request):
    return render(request, 'gateway.html')


def reject(request):
    messages.error(request, 'Ha ocurrido un error, por favor vuelve a intentarlo.')
    return HttpResponseRedirect(reverse('cart'))


def confirmation(request):
    order = Order.objects.filter(user_id=request.user.id, status='Paid').order_by('order_id').reverse().first()
    products = Cart.objects.filter(order_id=order.order_id)
    total = 0
    for p in products:
        total += p.price
    return render(request, 'confirmation.html', {'order': order, 'products': products, 'total': total})


def my_address(request):
    address = UserAddress.objects.filter(user_id=request.user.id, enabled=True)
    return render(request, 'my_address.html', {'address': address})


def my_orders(request):
    orders = Order.objects.filter(user_id=request.user.id, status='Paid')
    return render(request, 'my_orders.html', {'orders': orders})


def mis_solicitudes_de_compra(request):
    if request.user.is_authenticated and request.user.user_type == 1:
        return render(request, 'c_externo_mis_solicitudes_de_compra.html', {})
    else:
        return render(request, 'login.html', {})


@user_passes_test(lambda u: u.is_superuser)
def mantenedor_solicitudes_compra(request):
    if request.user.is_authenticated:
        return render(request, 'mantenedor_solicitudes_compra.html', {})
    else:
        return render(request, 'login.html', {})


@user_passes_test(lambda u: u.is_superuser)
def mantenedor(request):
    if request.user.is_authenticated:
        return render(request, 'mantenedor.html', {})
    else:
        return render(request, 'login.html', {})


@user_passes_test(lambda u: u.is_superuser)
def procesos_admin(request):
    if request.user.is_authenticated:
        return render(request, 'procesos_admin.html', {})
    else:
        return render(request, 'login.html', {})


@receiver(post_save, sender=User)
def set_new_user_inactive_transportista(sender, instance, created, **kwargs):
    if instance.user_type == 3:
        if created:
            instance.is_active = False


@receiver(post_save, sender=User)
def set_new_user_inactive_productor(sender, instance, created, **kwargs):
    if instance.user_type == 5:
        if created:
            instance.is_active = False
