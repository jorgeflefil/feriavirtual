from rest_framework import serializers

from .models import *


class UserSerializer(serializers.HyperlinkedModelSerializer):
    url = serializers.HyperlinkedIdentityField(view_name="usuarios-detail")
    user_type = serializers.CharField(source='get_user_type_display')

    class Meta:
        model = User
        fields = ('url', 'id', 'first_name', 'last_name', 'email', 'user_type', 'is_active')
        datatables_always_serialize = ('id', 'first_name', 'last_name', 'email', 'user_type', 'is_active')


class ProductoSerializer(serializers.HyperlinkedModelSerializer):
    url = serializers.HyperlinkedIdentityField(view_name="productos-detail")

    class Meta:
        model = Producto
        fields = ('url', 'id', 'nombre_producto', 'imagen_producto',)
        datatables_always_serialize = ('id', 'nombre_producto')


class ListaProductoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Producto
        fields = ('id', 'nombre_producto', 'imagen_producto',)


class MejorPujaSerializer(serializers.ModelSerializer):
    class Meta:
        model = PostulacionTransportista
        fields = ('id_subasta', 'id_transportista', 'valor_puja', 'estado_postulacion')
        datatables_always_serialize = ('id_subasta', 'id_transportista', 'valor_puja', 'estado_postulacion')


class ProductoImgSerializer(serializers.ModelSerializer):
    class Meta:
        model = Producto
        fields = ('imagen_producto',)


class ProductorSerializer(serializers.ModelSerializer):
    url = serializers.HyperlinkedIdentityField(view_name="productores-detail")
    user = UserSerializer()

    class Meta:
        model = Productor
        fields = ('url', 'pk', 'user',)


class ContratoProductorSerializer(serializers.ModelSerializer):
    url = serializers.HyperlinkedIdentityField(view_name="contrato_productor-detail")

    class Meta:
        model = ContratoProductor
        fields = ('url', 'pk', 'productor', 'fecha_inicio', 'fecha_termino', 'estado')


class TransportistaSerializer(serializers.ModelSerializer):
    url = serializers.HyperlinkedIdentityField(view_name="transportistas-detail")
    user = UserSerializer()

    class Meta:
        model = Transportista
        fields = ('url', 'user',)


class ClienteExternoSerializer(serializers.ModelSerializer):
    class Meta:
        model = ClienteExterno
        fields = ('pk', 'user',)


class ClienteInternoSerializer(serializers.ModelSerializer):
    class Meta:
        model = ClienteInterno
        fields = ('pk', 'user',)


class CartSerializer(serializers.ModelSerializer):
    user_id = UserSerializer
    product_id = ListaProductoSerializer()
    quantity = serializers.IntegerField(allow_null=False)

    class Meta:
        model = Cart
        fields = ('id', 'product_id', 'user_id', 'quantity')


class ProductorProductoCalidadSerializer(serializers.Serializer):
    producto__nombre_producto = serializers.CharField()
    cantidad__sum = serializers.IntegerField()


class ProductorProductoSerializer(serializers.ModelSerializer):
    url = serializers.HyperlinkedIdentityField(view_name="productor_productos-detail")
    productor = serializers.HyperlinkedRelatedField(
        read_only=True,
        view_name='productores-detail'
    )
    productor = ProductorSerializer
    producto = ListaProductoSerializer()
    calidad_str = serializers.CharField(source='get_calidad_display')

    class Meta:
        model = ProductorProducto
        fields = (
            'url', 'pk', 'productor', 'producto', 'precio_unitario', 'cantidad', 'calidad', 'calidad_str', 'interna', 'unidad')
        datatables_always_serialize = (
            'pk', 'productor', 'producto', 'precio_unitario', 'cantidad', 'calidad', 'calidad_str', 'interna', 'unidad')


class ModifProductorProductoSerializer(serializers.ModelSerializer):
    producto = ProductoSerializer

    class Meta:
        model = ProductorProducto
        fields = ('url', 'pk', 'productor', 'producto', 'precio_unitario', 'cantidad', 'calidad',)
        datatables_always_serialize = ('pk', 'productor', 'producto', 'precio_unitario', 'cantidad', 'calidad',)


class SolicitudCompraProductoRequeridoSerializer(serializers.ModelSerializer):
    def __init__(self, *args, **kwargs):
        many = kwargs.pop('many', True)
        super(SolicitudCompraProductoRequeridoSerializer, self).__init__(many=many, *args, **kwargs)

    calidad_str = serializers.CharField(source='get_calidad_display')

    class Meta:
        model = ProductoRequerido
        fields = ('producto', 'cantidad', 'calidad', 'calidad_str')


class DetalleSolicitudDeCompraSerializer(serializers.ModelSerializer):
    cliente_externo = ClienteExternoSerializer(required=False, allow_null=True)
    productos_requeridos = SolicitudCompraProductoRequeridoSerializer(many=True, read_only=True)

    class Meta:
        model = SolicitudDeCompra
        fields = ('pk', 'cliente_externo', 'fecha_solicitud', 'estado_solicitud', 'productos_requeridos')


class SolicitudDeCompraSerializer(serializers.ModelSerializer):
    cliente_externo = ClienteExternoSerializer(required=False, allow_null=True)

    class Meta:
        model = SolicitudDeCompra
        fields = ('pk', 'cliente_externo', 'fecha_solicitud', 'estado_solicitud')


class PedidoSerializer(serializers.ModelSerializer):
    cliente_interno = ClienteInternoSerializer

    class Meta:
        model = Pedido
        fields = ('pk', 'cliente_interno', 'fecha_solicitud', 'estado_pedido')


class ProductoRequeridoSerializer(serializers.ModelSerializer):
    def __init__(self, *args, **kwargs):
        many = kwargs.pop('many', True)
        super(ProductoRequeridoSerializer, self).__init__(many=many, *args, **kwargs)

    producto = ProductoSerializer()
    calidad_str = serializers.CharField(source='get_calidad_display')

    class Meta:
        model = ProductoRequerido
        fields = ('pk', 'solicitud_compra', 'producto', 'cantidad', 'calidad', 'calidad_str')


class ProcesoVentaSerializer(serializers.ModelSerializer):
    solicitud_compra = SolicitudDeCompraSerializer(required=False, allow_null=True)
    pedido = PedidoSerializer(required=False, allow_null=True)
    transportista = TransportistaSerializer()

    class Meta:
        model = ProcesoVenta
        fields = ('pk', 'estado_proceso', 'fecha_creacion', 'fecha_termino',
                  'solicitud_compra', 'pedido', 'transportista')
        datatables_always_serialize = (
            'pk', 'estado_proceso', 'fecha_creacion', 'fecha_termino', 'solicitud_compra', 'pedido')


class ProcesoPostulacionSerializer(serializers.ModelSerializer):
    proceso_venta = ProcesoVentaSerializer()

    class Meta:
        model = ProcesoPostulacion
        fields = ('pk', 'fecha_inicio_postulacion', 'fecha_fin_postulacion', 'estado_proceso', 'proceso_venta')


class PostulacionProductorSerializer(serializers.ModelSerializer):
    proceso_postulacion = ProcesoPostulacionSerializer()
    productor = ProductorSerializer()

    class Meta:
        model = PostulacionProductor
        fields = ('pk', 'proceso_postulacion', 'productor', 'estado_postulacion')


class DetalleProcesoPostulacionSerializer(serializers.ModelSerializer):
    proceso_venta = ProcesoVentaSerializer()
    postulacion_productores = PostulacionProductorSerializer(many=True, read_only=True,
                                                             source='postulacionproductor_set')

    class Meta:
        model = ProcesoPostulacion
        fields = ('pk', 'fecha_inicio_postulacion', 'fecha_fin_postulacion', 'estado_proceso', 'proceso_venta',
                  'postulacion_productores')


class ListaSubastaSerializer(serializers.ModelSerializer):
    url = serializers.HyperlinkedIdentityField(view_name="subastas_admin-detail")
    proceso = ProcesoVentaSerializer()
    transportista = TransportistaSerializer(required=False, allow_null=True)

    class Meta:
        model = Subasta
        fields = (
            'url', 'id_subasta', 'fecha_creacion', 'fecha_termino', 'direccion_retiro', 'direccion_entrega', 'estado_subasta',
            'proceso', 'transportista')
        datatables_always_serialize = ('id_subasta','proceso', 'direccion_retiro', 'direccion_entrega')


class ListaPujaSerializer(serializers.ModelSerializer):
    class Meta:
        model = PostulacionTransportista
        fields = ('id_subasta', 'id_transportista', 'valor_puja', 'estado_postulacion')
        datatables_always_serialize = ('id_subasta', 'id_transportista', 'valor_puja', 'estado_postulacion')


class ListaPujaAdminSerializer(serializers.ModelSerializer):
    url = serializers.HyperlinkedIdentityField(view_name="pujas_admin-detail")
    id_subasta = ListaSubastaSerializer()
    id_transportista = TransportistaSerializer()

    class Meta:
        model = PostulacionTransportista
        fields = ('url', 'pk', 'id_subasta', 'id_transportista', 'valor_puja', 'estado_postulacion')
        datatables_always_serialize = ('id_subasta', 'id_transportista', 'valor_puja', 'estado_postulacion')
