from celery import shared_task
from django.db.models import Avg, FloatField, Q, Sum, IntegerField
from datetime import date

from app.models import *


@shared_task()
def determinar_ganadores_postulacion_productores():
    today = datetime.today()

    # OBTENER TODOS LOS PROCESOS DE POSTULACION CON ESTADO "EN PROCESO"
    procesos_de_postulacion = ProcesoPostulacion.objects.filter(Q(
        estado_proceso="En proceso de postulación de productores") & Q(fecha_fin_postulacion__lte=today))

    for proceso_postulacion in procesos_de_postulacion:

        # MOSTRAR PROCESO DE POSTULACION A ITERAR (PROCESO DE VENTA)
        print("---------- Proceso #" + str(proceso_postulacion.pk) + " ----------")

        # CLIENTE DE PROCESO DE VENTA
        cliente_externo = proceso_postulacion.proceso_venta.solicitud_compra.cliente_externo.user
        print("Cliente: " + cliente_externo.first_name + " " + cliente_externo.last_name)

        # PRODUCTOS REQUERIDOS
        productos_requeridos = proceso_postulacion.proceso_venta.solicitud_compra.productos_requeridos.all().values('producto')

        prod_requeridos_cantidad = proceso_postulacion.proceso_venta.solicitud_compra.productos_requeridos.all().values('producto',
                                                                                                                         'cantidad')
        print(prod_requeridos_cantidad)

        # POSTULACION PRODUCTORES
        postulaciones_productores = proceso_postulacion.postulacionproductor_set.all().values('productor')

        # SI PROCESO TIENE POSTULACIONES
        if postulaciones_productores:

            # OBTENER SUMA TOTAL DE PRODUCTOS DISPONIBLES
            cantidad_productos_productor = ProductorProducto.objects.filter(
                Q(productor__in=postulaciones_productores) & Q(producto__in=productos_requeridos)).values(
                'producto').annotate(cantidad_total=Sum('cantidad', output_field=IntegerField())).order_by('producto')

            # VERIFICAR POSIBILIDAD DE CUMPLIR EL PEDIDO CON LOS PRODUCTOS DISPONIBLES
            tabla_verdad_productos = []
            for prod_req in prod_requeridos_cantidad:
                for prod_cantidad in cantidad_productos_productor:
                    if prod_req['producto'] == prod_cantidad['producto']:
                        if prod_req['cantidad'] <= prod_cantidad['cantidad_total']:
                            tabla_verdad_productos.append(True)
                        else:
                            tabla_verdad_productos.append(False)

            # SI TABLA DE VERDAD TIENE VALORES
            if len(tabla_verdad_productos) != 0:

                cumple_productos = all(tabla_verdad_productos)
                print("¿Cumple con productos requeridos?: " + str(cumple_productos))

                # SI CUMPLE CON TODOS LOS PRODUCTOS
                if cumple_productos:

                    # CALCULAR MEJORES OFERTAS DE VENTA
                    lista_productores_mejor_promedio = ProductorProducto.objects.filter(
                        Q(productor__in=postulaciones_productores) & Q(producto__in=productos_requeridos)).values(
                        'productor').annotate(promedio=Avg('precio_unitario', output_field=FloatField())).order_by('promedio')
                    print(lista_productores_mejor_promedio)

                    # ITERAR LISTA DE PRODUCTORES CON MEJOR PROMEDIO DE PRODUCTOS
                    id_productores_seleccionados = []
                    for productor in lista_productores_mejor_promedio:
                        id_productores_seleccionados.append(productor['productor'])

                        total_productor = ProductorProducto.objects.filter(
                            Q(productor=productor['productor']) & Q(producto__in=productos_requeridos)).values(
                            'producto', 'calidad', 'precio_unitario').annotate(
                            cantidad_total=Sum('cantidad', output_field=IntegerField())).order_by('producto')

                        total_productor = list(total_productor)
                        print(total_productor)
                        prod_requeridos_cantidad = list(prod_requeridos_cantidad)

                        tabla_verdad_productos_seleccionados = []
                        for prod_req in prod_requeridos_cantidad:
                            for prod_cantidad in total_productor:
                                if prod_req['producto'] == prod_cantidad['producto']:
                                    if prod_req['cantidad'] > 0:

                                        productor_envio = Productor.objects.get(pk=productor['productor'])
                                        postulacion_productor = PostulacionProductor.objects.get(
                                            proceso_postulacion=proceso_postulacion.pk, productor=productor_envio)

                                        envio_producto = EnvioProductoProductor()
                                        envio_producto.postulacion_productor = postulacion_productor
                                        envio_producto.productor = productor_envio
                                        envio_producto.producto = Producto.objects.get(pk=prod_cantidad['producto'])
                                        envio_producto.precio_unitario = prod_cantidad['precio_unitario']
                                        envio_producto.calidad = prod_cantidad['calidad']

                                        if prod_req['cantidad'] >= prod_cantidad['cantidad_total']:
                                            envio_producto.cantidad = prod_cantidad['cantidad_total']
                                        else:
                                            envio_producto.cantidad = prod_req['cantidad']
                                        envio_producto.save()

                                        print(str(envio_producto.postulacion_productor) + " " + str(
                                            envio_producto.productor.user.first_name) + " " + str(
                                            envio_producto.producto.nombre_producto + " " + str(envio_producto.precio_unitario) + " " + str(
                                                envio_producto.calidad) + " " + str(envio_producto.cantidad)))

                                    # RESTAR PRODUCTO REQUERIDO
                                    prod_req['cantidad'] = prod_req['cantidad'] - prod_cantidad['cantidad_total']

                                    # SI EL PRODUCTO ES CUMPLIDO ELIMINAR DE LISTA DE REQUERIDOS
                                    if prod_req['cantidad'] <= 0:
                                        tabla_verdad_productos_seleccionados.append(True)
                                        # prod_requeridos_cantidad.remove(prod_req)
                                    else:
                                        tabla_verdad_productos_seleccionados.append(False)

                        cumple_productos = all(tabla_verdad_productos_seleccionados)
                        if cumple_productos:
                            break

                    print(proceso_postulacion.estado_proceso)
                    print(id_productores_seleccionados)

                    for productor in id_productores_seleccionados:
                        postulacion_productor = PostulacionProductor.objects.get(
                            proceso_postulacion=proceso_postulacion.pk, productor__pk=productor)
                        postulacion_productor.estado_postulacion = "Ganador"
                        postulacion_productor.save()

                    print(proceso_postulacion.pk)

                    proceso_de_postulacion = ProcesoPostulacion.objects.get(pk=proceso_postulacion.pk)
                    proceso_de_postulacion.estado_proceso = "Productores seleccionados"
                    proceso_de_postulacion.save()

                    proceso_de_venta = ProcesoVenta.objects.get(pk=proceso_postulacion.proceso_venta.pk)
                    proceso_de_venta.estado_proceso = "Productores seleccionados"
                    proceso_de_venta.save()

            else:
                print("No hay productos disponibles, añadir más dias")
                proceso_postulacion.fecha_fin_postulacion = proceso_postulacion.fecha_fin_postulacion + timedelta(days=1)
        else:
            print("No hay postulantes, añadir más dias")
            proceso_postulacion.fecha_fin_postulacion = proceso_postulacion.fecha_fin_postulacion + timedelta(days=1)

    return None


@shared_task()
def actualizar_vigencia_productor():
    today = date.today()
    contrato_productores = ContratoProductor.objects.filter(fecha_termino__lt=today)
    for contrato in contrato_productores:
        contrato.estado = "Expirado"
        contrato.save()