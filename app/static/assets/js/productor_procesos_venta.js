function ListarProcesosVentaAsignados(productor_id) {
    $('#lista_procesos_admin').DataTable({
        ordering: true,
        info: false,
        fixedHeader: {
            header: true,
        },
        scrollCollapse: true,
        paging: false,
        ajax: "/api/mis_postulaciones_productor/?format=datatables",
        columns: [
            {
                data: "proceso_postulacion.proceso_venta.pk",
                searchable: true,
            },
            {
                data: "proceso_postulacion.proceso_venta.estado_proceso",
                searchable: true,
            },
            {
                data: "proceso_postulacion.proceso_venta.fecha_creacion",
                searchable: true,
            },
            {
                data: "proceso_postulacion.proceso_venta.fecha_termino",
                searchable: true,
            },
        ],
        language: {
            url: "//cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json"
        },
        columnDefs: [{
            targets: 4,
            className: "desktop",
            render: function (data, type, row) {
                return '<button type="button" class="btn btn-light border-dark btn-sm" data-toggle="modal"' +
                    ' data-target="#modal_postulantes_proceso_venta" href="#" onclick="return CargarDetalleProcesoPostulacion(' + row.proceso_postulacion.pk + ', ' +
                    '' + row.proceso_postulacion.proceso_venta.solicitud_compra.cliente_externo.pk + ', ' + row.proceso_postulacion.proceso_venta.solicitud_compra.pk + ')">' +
                    '<i class="fa fa-lg fa-apple"></i>\n' +
                    '</button>\n'
            }
        }, {
            targets: 5,
            className: "desktop",
            render: function (data, type, row) {
                return '<button type="button" class="btn btn-light border-dark btn-sm" data-toggle="modal"' +
                    ' data-target="#modal_postulantes" href="#" onclick="return InformeProcesoVenta(' + row.proceso_postulacion.proceso_venta.pk + ')">' +
                    '<i class="fa fa-lg  fa-file-text-o"></i>\n' +
                    '</button>\n'
            }
        }],
    })
}


function CargarDetalleProcesoPostulacion(id_proceso_postulacion_productores, id_cliente, id_solicitud) {
    $("#id_proceso_postulacion_productores_mostrar_modal").text("#" + id_proceso_postulacion_productores)
    CargarNombreCliente(id_cliente)

    $.ajax({
        url: "/api/detalle_procesos_admin/" + id_proceso_postulacion_productores + "/",
        type: 'GET',
        success: function (data) {
            console.log(data)
            $("#id_fecha_inicio_proceso_venta").val(data.fecha_inicio_postulacion)
            $("#id_fecha_fin_proceso_venta").val(data.fecha_fin_postulacion)
            $("#id_estado_proceso_venta").val(data.estado_proceso)

        }
    })

    let datatable = $('#table_detalle_proceso_venta_de_compra').DataTable({
        ordering: true,
        info: false,
        serverSide: false,
        scrollCollapse: true,
        paging: false,
        cache: false,
        destroy: true,
        searching: false,
        ajax: '/api/producto_requerido/?format=datatables&solicitud_compra=' + id_solicitud,
        columns: [
            {
                data: null,
            },
            {
                data: "producto.nombre_producto",
                className: "text-capitalize",
            },
            {
                data: "cantidad",
                className: "text-capitalize",
            },
            {
                data: "calidad_str",
                className: "text-capitalize",
            },
        ],
        columnDefs: [{
            searchable: false,
            orderable: false,
            targets: 0
        }],
        order: [[1, 'asc']],
        language: {
            url: "//cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json"
        },
    })

    datatable.on('order.dt search.dt', function () {
        datatable.column(0, {search: 'applied', order: 'applied'}).nodes().each(function (cell, i) {
            cell.innerHTML = i + 1;
        });
    }).draw();
}

function CargarNombreCliente(id_cliente) {
    $.ajax({
        url: "/api/clientes_externos/" + id_cliente + "/",
        type: 'GET',
        success: function (data) {
            console.log(data)
            $("#cliente").val(data.id)
            $("#id_cliente_proceso_venta").val(data.first_name + " " + data.last_name)
        }
    })
}



