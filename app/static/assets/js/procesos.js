function ListarProcesosAdmin() {
    $('#lista_procesos_admin').DataTable({
        ordering: true,
        info: false,
        fixedHeader: {
            header: true,
        },
        scrollCollapse: true,
        paging: false,
        ajax: "/api/procesos_ventas/?format=datatables",
        columns: [
            {
                data: "pk",
                searchable: true,
            },
            {
                data: "estado_proceso",
                searchable: true,
            },
            {
                data: "fecha_creacion",
                searchable: true,
            },
            {
                data: "fecha_termino",
                searchable: true,
            },
        ],
        language: {
            url: "//cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json"
        },
        columnDefs: [{
            targets: 4,
            className: "desktop",
            render: function (data, type, row) {
                return '<button type="button" class="btn btn-primary" data-toggle="modal"' +
                    ' data-target="#modal_detalle_solicitud_compra" href="#" onclick="return CargarDetalleSolicitud(' + row.pk + ', ' + row.solicitud_compra.cliente_externo.pk + ', ' + row.solicitud_compra.pk + ')">' +
                    'Ver detalles</button>';
            },
        }, {
            targets: 5,
            className: "desktop",
            render: function (data, type, row) {
                return '<button type="button" class="btn btn-light border-dark btn-sm" data-toggle="modal"' +
                    ' data-target="#modal_postulantes" href="#" onclick="return InformeProcesoVenta(' + row.pk + ')">' +
                    '<i class="fa fa-lg  fa-file-text-o"></i>\n' +
                    '</button>\n'
            }
        }],
    });
}

function CargarDetalleSolicitud(id_proceso, id_cliente, id_solicitud) {
    $("#modal_asignarLabel").text("Proceso #" + id_proceso);
    CargarNombreCliente(id_cliente);
    CargarDetalleProceso(id_proceso);
    CargarDetalleProcesoPostulacion(id_proceso)

    $.ajax({
        url: "/api/detalle_solicitud_de_compra/" + id_solicitud + "/",
        type: 'GET',
        success: function (data) {
            console.log(data);
            $("#id_fecha_solicitud").val(data.fecha_solicitud);
            $("#id_estado_solicitud").val(data.estado_solicitud);
        }
    })

    /* PRODUCTOS REQUERIDOS */
    let datatable = $('#table_detalle_solicitud_de_compra').DataTable({
        ordering: true,
        info: false,
        serverSide: false,
        scrollCollapse: true,
        paging: false,
        cache: false,
        destroy: true,
        searching: false,
        ajax: '/api/producto_requerido/?format=datatables&solicitud_compra=' + id_solicitud,
        columns: [
            {
                data: null,
            },
            {
                data: "producto.nombre_producto",
                className: "text-capitalize",
            },
            {
                data: "cantidad",
                className: "text-capitalize",
            },
            {
                data: "calidad_str",
                className: "text-capitalize",
            },
        ],
        columnDefs: [{
            searchable: false,
            orderable: false,
            targets: 0
        }],
        order: [[1, 'asc']],
        language: {
            url: "//cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json"
        },columnDefs: [{
            searchable: false,
            orderable: false,
            targets: 0
        }],
        order: [[1, 'asc']],
        language: {
            url: "//cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json"
        },
    })

    datatable.on('order.dt search.dt', function () {
        datatable.column(0, {search: 'applied', order: 'applied'}).nodes().each(function (cell, i) {
            cell.innerHTML = i + 1;
        });
    }).draw();
}

function CargarNombreCliente(id_cliente) {
    $.ajax({
        url: "/api/clientes_externos/" + id_cliente + "/",
        type: 'GET',
        success: function (data) {
            console.log(data);
            $("#cliente_externo").val(data.id);
            $("#id_cliente_solicitud").val(data.first_name + " " + data.last_name);
        }
    })
}

function CargarDetalleProceso(id_proceso) {
    $.ajax({
        url: "/api/procesos_ventas/" + id_proceso + "/",
        type: 'GET',
        success: function (data) {
            if (data.transportista == null) {
                $("#modal_detalle_solicitud_compra .transportistas").hide();
                $("#transportista_title").text("Transportista sin asignar");

            } else {
                $("#modal_detalle_solicitud_compra .transportistas").show();
                $("#transportista_title").text("Transportista asignado");
                $("#id_nombre_transportista").val(data.transportista.user.first_name + ' ' + data.transportista.user.last_name);
                $("#id_fecha_entrega").val(data.fecha_termino);
                $("#id_correo_transportista").val(data.transportista.user.email);
                $("#id_estado_proceso").val(data.estado_proceso);
            }
        }
    });
}

function CargarDetalleProcesoPostulacion(id_proceso_postulacion_productores) {


    let datatable = $('#table_detalle_proceso_venta_de_compra').DataTable({
        ordering: true,
        info: false,
        serverSide: false,
        scrollCollapse: true,
        paging: false,
        cache: false,
        destroy: true,
        searching: false,
        ajax: '/api/postulacion_productor/?format=datatables&proceso_postulacion=' + id_proceso_postulacion_productores,
        columns: [
            {
                data: null,
            },
            {
                data: "productor.user",
                className: "text-capitalize",
            },
            {
                data: "estado_postulacion",
                className: "text-capitalize",
            },
        ],
        columnDefs: [{
            searchable: false,
            orderable: false,
            targets: 0
        }, {
            targets: 1,
            render: function (data) {
                return data.first_name + " " + data.last_name
            }
        }
        ],
        order: [[1, 'asc']],
        language: {
            url: "//cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json"
        },
    })

    datatable.on('order.dt search.dt', function () {
        datatable.column(0, {search: 'applied', order: 'applied'}).nodes().each(function (cell, i) {
            cell.innerHTML = i + 1;
        });
    }).draw();
}
