function ListarProcesosVentaAsignados(productor_id) {
    $('#lista_procesos_admin').DataTable({
        ordering: true,
        info: false,
        fixedHeader: {
            header: true,
        },
        scrollCollapse: true,
        paging: false,
        ajax: "/api/mis_procesos_venta_cliente_externo/?format=datatables",
        columns: [
            {
                data: "proceso_venta.pk",
                searchable: true,
            },
            {
                data: "proceso_venta.estado_proceso",
                searchable: true,
            },
            {
                data: "proceso_venta.fecha_creacion",
                searchable: true,
            },
            {
                data: "proceso_venta.fecha_termino",
                searchable: true,
            },
        ],
        language: {
            url: "//cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json"
        },
        columnDefs: [{
            targets: 4,
            className: "desktop",
            render: function (data, type, row) {
                return '<button type="button" class="btn btn-light border-dark btn-sm" data-toggle="modal"' +
                    ' data-target="#modal_postulantes_proceso_venta" href="#" onclick="return CargarDetalleProcesoPostulacion(' + row.pk + ', ' +
                    '' + row.proceso_venta.solicitud_compra.cliente_externo.pk + ',' + row.proceso_venta.solicitud_compra.pk + ')">' +
                    '<i class="fa fa-lg fa-users"></i>\n' +
                    '</button>\n'
            }
        }, {
            targets: 5,
            className: "desktop",
            render: function (data, type, row) {
                return '<button type="button" class="btn btn-light border-dark btn-sm" data-toggle="modal"' +
                    ' data-target="#modal_postulantes" href="#" onclick="return InformeProcesoVenta(' + row.proceso_venta.pk + ')">' +
                    '<i class="fa fa-lg  fa-file-text-o"></i>\n' +
                    '</button>\n'
            },
        }, {
            targets: 6,
            className: "desktop",
            render: function (data, type, row) {
                if (row.proceso_venta.estado_proceso == "En proceso de despacho") {
                    return '<button type="button" class="btn btn-success" data-toggle="modal"' +
                        ' data-target="#id_modal_recibo" href="#" onclick="return MarcarRecibido(' + row.proceso_venta.pk + ')">' +
                        '\n' +
                        'Marcar como recibido</button>\n';
                }else{
                    return '<button type="button" class="btn btn-success" data-toggle="modal"' +
                        ' data-target="#id_modal_recibo" href="#" onclick="return MarcarRecibido(' + row.proceso_venta.pk + ')" disabled>' +
                        '\n' + 'Marcar como recibido</button>\n';
                }
            },
        }],
    })
}


function CargarDetalleProcesoPostulacion(id_proceso_postulacion_productores, id_cliente, id_solicitud) {
    $("#id_proceso_postulacion_productores_mostrar_modal").text("#" + id_proceso_postulacion_productores)
    CargarNombreCliente(id_cliente)
    CargarDetalleProductosRequeridos(id_solicitud)
    CargarTransportista(id_proceso_postulacion_productores)

    $.ajax({
        url: "/api/detalle_procesos_admin/" + id_proceso_postulacion_productores + "/",
        type: 'GET',
        success: function (data) {
            console.log(data)
            $("#id_fecha_inicio_proceso_venta").val(data.fecha_inicio_postulacion)
            $("#id_fecha_fin_proceso_venta").val(data.fecha_fin_postulacion)
            $("#id_estado_proceso_venta").val(data.estado_proceso)
        }
    })


    let datatable = $('#table_detalle_proceso_venta_de_compra').DataTable({
        ordering: true,
        info: false,
        serverSide: false,
        scrollCollapse: true,
        paging: false,
        cache: false,
        destroy: true,
        searching: false,
        ajax: '/api/postulacion_productor/?format=datatables&proceso_postulacion=' + id_proceso_postulacion_productores,
        columns: [
            {
                data: null,
            },
            {
                data: "productor.user",
                className: "text-capitalize",
            },
            {
                data: "estado_postulacion",
                className: "text-capitalize",
            },
        ],
        columnDefs: [{
            searchable: false,
            orderable: false,
            targets: 0
        }, {
            targets: 1,
            render: function (data, type, row) {
                return data.first_name + " " + data.last_name
            }
        }
        ],
        order: [[1, 'asc']],
        language: {
            url: "//cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json"
        },
    })

    datatable.on('order.dt search.dt', function () {
        datatable.column(0, {search: 'applied', order: 'applied'}).nodes().each(function (cell, i) {
            cell.innerHTML = i + 1;
        });
    }).draw();
}

function CargarNombreCliente(id_cliente) {
    $.ajax({
        url: "/api/clientes_externos/" + id_cliente + "/",
        type: 'GET',
        success: function (data) {
            console.log(data)
            $("#cliente").val(data.id)
            $("#id_cliente_proceso_venta").val(data.first_name + " " + data.last_name)
        }
    })
}

function MarcarRecibido(id_proceso) {
    console.log(id_proceso)
    jQuery("#id_form_recibir").submit(function (event) {
        event.preventDefault();
        let settings = {
            url: "/api/procesos_ventas/" + id_proceso + "/update_recibir/",
            method: "PUT",
            cache: false,
            contentType: false,
            processData: false,
            beforeSend: function (xhr) {
                xhr.setRequestHeader("X-CSRFToken", csrftoken);
            },
            failure: function (data) {
                alert('Got an error');
            }
        };

        jQuery.ajax(settings).done(function (response) {
            jQuery(".mensajes").append("<div class=\"row\">\n" +
                "    <div class=\"col-12\">\n" +
                "        <div id=\"success-alert\" class=\"alert alert-success alert-dismissible fade show\" role=\"alert\">\n" +
                "            <strong>El pedido se marcó como recibido correctamente." + "\n" +
                "            <button aria-label=\"Close\" class=\"close\" data-dismiss=\"alert\" type=\"button\">\n" +
                "                <span aria-hidden=\"true\">&times;</span>\n" +
                "            </button>\n" +
                "        </div>\n" +
                "    </div>\n" +
                "</div>");
            $("#cerrar_modal").click();
            RecargarTablasAdmin();
        });
    });
}

function RecargarTablasAdmin() {
    jQuery('#lista_procesos_admin').DataTable().ajax.reload();
}


function CargarDetalleProductosRequeridos(id_solicitud) {
    /* PRODUCTOS REQUERIDOS */
    let datatable = $('#table_detalle_solicitud_de_compra').DataTable({
        ordering: true,
        info: false,
        serverSide: false,
        scrollCollapse: true,
        paging: false,
        cache: false,
        destroy: true,
        searching: false,
        ajax: '/api/producto_requerido/?format=datatables&solicitud_compra=' + id_solicitud,
        columns: [
            {
                data: null,
            },
            {
                data: "producto.nombre_producto",
                className: "text-capitalize",
            },
            {
                data: "cantidad",
                className: "text-capitalize",
            },
            {
                data: "calidad_str",
                className: "text-capitalize",
            },
        ],
        columnDefs: [{
            searchable: false,
            orderable: false,
            targets: 0
        }],
        order: [[1, 'asc']],
        language: {
            url: "//cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json"
        }, columnDefs: [{
            searchable: false,
            orderable: false,
            targets: 0
        }],
        order: [[1, 'asc']],
        language: {
            url: "//cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json"
        },
    })

    datatable.on('order.dt search.dt', function () {
        datatable.column(0, {search: 'applied', order: 'applied'}).nodes().each(function (cell, i) {
            cell.innerHTML = i + 1;
        });
    }).draw();
}

function CargarTransportista(id_proceso_venta){
    $.ajax({
        url: "/api/procesos_ventas/" + id_proceso_venta + "/",
        type: 'GET',
        success: function (data) {
            if (data.transportista == null) {
                $("#modal_postulantes_proceso_venta .transportistas").hide();
                $("#transportista_title").text("Transportista sin asignar");

            } else {
                $("#modal_postulantes_proceso_venta .transportistas").show();
                $("#transportista_title").text("Transportista asignado");
                $("#id_nombre_transportista").val(data.transportista.user.first_name + ' ' + data.transportista.user.last_name);
                $("#id_fecha_entrega").val(data.fecha_termino);
                $("#id_correo_transportista").val(data.transportista.user.email);
                $("#id_estado_proceso").val(data.estado_proceso);
            }
        }
    });
}