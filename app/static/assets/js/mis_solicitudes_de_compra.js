let lista_productos = {};
let lista_productos_seleccionados = [];
let filas = []

function CargarProductosInputSelect() {
    $.ajax({
        url: "/api/lista_productos/",
        dataType: "JSON",
        success: function (data) {
            lista_productos = data
        }
    })
}

function CargarNombreCliente(id_cliente) {
    $.ajax({
        url: "/api/clientes_externos/" + id_cliente + "/",
        type: 'GET',
        success: function (data) {
            $("#cliente_externo").val(data.id)
            $("#id_cliente_solicitud").val(data["first_name"] + " " + data["last_name"])
        }
    })
}


function CargarSolicitudCompra(id_solicitud, id_cliente) {
    id_solicitud_compra = id_solicitud;
    $("#id_solicitud_mostrar_modal").text("#" + id_solicitud)
    CargarNombreCliente(id_cliente)

    $.ajax({
        url: "/api/solicitud_de_compras/" + id_solicitud + "/",
        type: 'GET',
        success: function (data) {
            $("#id_fecha_solicitud").val(data.fecha_solicitud)
            $("#id_estado_solicitud").val(data.estado_solicitud)
            if (data.estado_solicitud == "En proceso") {
                $("#modal_detalle_solicitud_compra .modal-footer").show()
            } else {
                $("#modal_detalle_solicitud_compra .modal-footer").hide()
            }
        }
    })

    let datatable = $('#table_detalle_solicitud_de_compra').DataTable({
        ordering: true,
        info: false,
        serverSide: false,
        scrollCollapse: true,
        paging: false,
        cache: false,
        destroy: true,
        searching: false,
        ajax: '/api/producto_requerido/?format=datatables&solicitud_compra=' + id_solicitud,
        columns: [
            {
                data: null,
            },
            {
                data: "producto.nombre_producto",
                className: "text-capitalize",
            },
            {
                data: "cantidad",
                className: "text-capitalize",
            },
            {
                data: "calidad_str",
                className: "text-capitalize",
            },
        ],
        columnDefs: [{
            searchable: false,
            orderable: false,
            targets: 0
        }],
        order: [[1, 'asc']],
        language: {
            url: "//cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json"
        },
    })

    datatable.on('order.dt search.dt', function () {
        datatable.column(0, {search: 'applied', order: 'applied'}).nodes().each(function (cell, i) {
            cell.innerHTML = i + 1;
        });
    }).draw();
}

$(document).ready(function () {
    ListarProductosProductor();
    CargarProductosInputSelect();

    $('#btn_añadir_producto_solicitud').click(function () {
        let nueva_fila = "<tr> <td> <select id=\"producto\" name=\"producto\" class=\"selectpicker form-control\" data-live-search=\"true\" title=\"--Seleccionar--\"> </select> " +
            "</td> <td> <select class=\"form-control\" id=\"calidad\" name=\"calidad\"> <option disabled selected>--Seleccionar--</option> <option value=\"1\">Alta</option> <option value=\"2\">Media</option> <option value=\"3\">Baja</option> </select> " +
            "</td> <td><input type=\"number\" id=\"cantidad\" name=\"cantidad\" min=\"1\" class=\"form-control\"> </td> </tr><input type='hidden' name='solicitud_compra'>";

        filas.push(nueva_fila)

        $("#table_productos_solicitud tr:last").after(nueva_fila);

        /* Cargar opciones de productos e ingresarlas a select  */
        $.each(lista_productos, function (i, val) {
            let opcion = `<option value="${val.id}">${val["nombre_producto"]}</option>`;
            $("#table_productos_solicitud tr:last .selectpicker").append(opcion)
        });
        $(".selectpicker").selectpicker('refresh');
    });

    $("#btnLimpiarListaSolicitud").click(function () {
        if (confirm('¿Estas seguro de limpiar los datos ingresados?')) {
            $('#table_productos_solicitud tr:not(:first)').remove();
            filas = []
        } else {
            // Do nothing!
        }
    });

    $("#form_solicitud_compra").submit(function (event) {
        event.preventDefault();

        if (!filas.length) {
            alert("No se puede crear una solicitud de compra vacía.")
        } else {

            $("#form_solicitud_compra .form-control")

            let formData = new FormData();
            formData.append("estado_solicitud", "En proceso");

            let settings = {
                url: "/api/cliente_externo/solicitud_de_compra/create/",
                method: "POST",
                cache: false,
                contentType: false,
                processData: false,
                data: formData,
                beforeSend: function (xhr) {
                    xhr.setRequestHeader("X-CSRFToken", csrftoken);
                },
                success: function () {
                },
                failure: function () {
                    alert('Got an error');
                }
            };

            // INGRESAR LISTA DE PRODUCTOS SELECCIONADOS
            $.ajax(settings).done(function (response) {
                CrearProductosRequeridos(response.pk)
            });
        }
    });
});

function CrearProductosRequeridos(id_solicitud) {
    let formData = new FormData($("#form_solicitud_compra")[0]);

    formData.append("cantidad_productos", JSON.stringify(filas.length))
    formData.append("solicitud_compra", id_solicitud)

    let settings = {
        url: "/api/producto_requerido/",
        method: "POST",
        cache: false,
        contentType: false,
        processData: false,
        data: formData,
        beforeSend: function (xhr) {
            xhr.setRequestHeader("X-CSRFToken", csrftoken);
        },
        success: function () {
            $('.mensajes').append(MensajeRespuesta("alert-success", "Solicitud de compra creada con exito."))
            $('.mensajes').find(".alert:last").slideDown("slow");
            $('#table_solicitudes_de_compra').DataTable().ajax.reload();
            $('#table_productos_solicitud tr:not(:first)').remove();
            filas = []
        },
        failure: function () {
            $('.mensajes').append(MensajeRespuesta("alert-danger", "Error al crear solicitud de compra."))
            $('.mensajes').find(".alert:last").slideDown("slow");
        }
    };
    $.ajax(settings).done(function (response) {
        console.log(response)
    });
}

function ListarProductosProductor() {
    $('#table_solicitudes_de_compra').DataTable({
        ordering: true,
        info: false,
        serverSide: true,
        scrollCollapse: true,
        paging: false,
        cache: false,
        ajax: '/api/solicitud_de_compras/?format=datatables',
        columns: [
            {
                data: "pk",
                orderable: true
            },
            {
                data: "fecha_solicitud",
                className: "text-capitalize",

            },
            {
                data: "estado_solicitud",
                render: function (data) {
                    return '<span class="align-self-center">' + data + '</span>';
                },
            },
            {
                data: "cliente_externo.pk",
            },
        ],
        language: {
            url: "//cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json"
        },

        columnDefs: [{
            targets: 3,
            className: "desktop",
            render: function (data, type, row) {
                return `<button type="button" class="btn btn-primary border-dark btn-sm" data-toggle="modal" data-target="#modal_detalle_solicitud_compra" href="#" onclick="return CargarSolicitudCompra(${row.pk},${row.cliente_externo.pk});">Ver solicitud</button>`;
            }
        }],
    })
}