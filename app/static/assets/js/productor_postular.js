let productos_requeridos = []
let mis_productos = []
let cumple_requisitos = []

function VerificarValidezDeProductos(id_proceso_postulacion, id_solicitud) {
    jQuery("#proceso_postulacion").val(id_proceso_postulacion)

    cumple_requisitos = []

    CargarProductosRequeridos(id_solicitud)
    CargarProductosDeProductor()

    jQuery.each(productos_requeridos, function (index, value_requerido) {
        let productor_cumple_con_producto = false

        jQuery.each(mis_productos, function (index, value_mi_producto) {

            // Si el productor tiene el mismo producto requerido
            if (value_requerido.producto.id === value_mi_producto.producto.id) {
                if (value_mi_producto.cantidad > 0) {
                    // Si el producto cumple con la calidad minima
                    if (value_mi_producto.calidad <= value_requerido.calidad) {
                        productor_cumple_con_producto = true
                    }
                }
            }
        });

        if (productor_cumple_con_producto) {
            cumple_requisitos.push(true)
        } else {
            cumple_requisitos.push(false)
        }

    });

    mis_productos = []
    productos_requeridos = []
}

function CargarProductosRequeridos(id_solicitud) {
    jQuery.ajax({
        url: `/api/productor_producto_requerido/?solicitud_compra=${id_solicitud}`,
        type: 'GET',
        async: false,
        success: function (data) {
            for (let i = 0; i < data.length; i++) {
                productos_requeridos.push(data[i])
            }
        }
    })
}

function CargarProductosDeProductor() {
    jQuery.ajax({
        url: `/api/productor_productos/`,
        type: 'GET',
        async: false,
        success: function (data) {
            for (let i = 0; i < data.length; i++) {
                mis_productos.push(data[i])
            }
        }
    })
}


function CargarDetalleSolicitud(id_solicitud, id_cliente) {
    id_solicitud_compra = id_solicitud;
    $("#id_solicitud_mostrar_modal").text("#" + id_solicitud)
    CargarNombreCliente(id_cliente)

    $.ajax({
        url: "/api/all_solicitud_de_compras/" + id_solicitud + "/",
        type: 'GET',
        success: function (data) {
            $("#id_fecha_solicitud").val(data.fecha_solicitud)
            $("#id_estado_solicitud").val(data.estado_solicitud)
            if (data.estado_solicitud === "En proceso") {
                $("#modal_detalle_solicitud_compra .modal-footer").show()
            } else {
                $("#modal_detalle_solicitud_compra .modal-footer").hide()
            }
        }
    })

    const datatable = $('#table_detalle_solicitud_de_compra').DataTable({
        ordering: true,
        info: false,
        serverSide: false,
        scrollCollapse: true,
        paging: false,
        cache: false,
        destroy: true,
        searching: false,
        ajax: '/api/producto_requerido/?format=datatables&solicitud_compra=' + id_solicitud,
        columns: [
            {
                data: null,
            },
            {
                data: "producto.nombre_producto",
                className: "text-capitalize",
            },
            {
                data: "cantidad",
                className: "text-capitalize",
            },
            {
                data: "calidad_str",
                className: "text-capitalize",
            },
        ],
        columnDefs: [{
            searchable: false,
            orderable: false,
            targets: 0
        }],
        order: [[1, 'asc']],
        language: {
            url: "//cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json"
        },
    })

    datatable.on('order.dt search.dt', function () {
        datatable.column(0, {search: 'applied', order: 'applied'}).nodes().each(function (cell, i) {
            cell.innerHTML = i + 1;
        });
    }).draw();
}

function CargarNombreCliente(id_cliente) {
    $.ajax({
        url: "/api/clientes_externos/" + id_cliente + "/",
        type: 'GET',
        success: function (data) {
            $("#cliente_externo").val(data.id)
            $("#id_cliente_solicitud").val(data.first_name + " " + data.last_name)
        }
    })
}

function ListarProcesosProductor(user_id) {
    $('#lista_procesos_productor').DataTable({
        ordering: true,
        info: false,
        fixedHeader: {
            header: true,
        },
        scrollCollapse: true,
        paging: false,
        ajax: "/api/detalle_proceso_postulacion_productor/?format=datatables",
        columns: [
            {
                data: "pk",
                searchable: true,
            },
            {
                data: "estado_proceso",
                searchable: true,
            },
            {
                data: "fecha_inicio_postulacion",
                searchable: true,
            },
            {
                data: "fecha_fin_postulacion",
                searchable: true,
            },
            {
                data: "proceso_venta.solicitud_compra",
                searchable: true,
                render: function (data, type, row) {
                    return '<button type="button" class="btn btn-link" data-toggle="modal" data-target="#modal_detalle_solicitud_compra" href="#" onclick="return CargarDetalleSolicitud(' + row.proceso_venta.solicitud_compra.pk + ',' + row.proceso_venta.solicitud_compra.cliente_externo.pk + ');">' +
                        'Ver detalle' +
                        '</button>';
                },
            },
            {
                data: null,
            },
        ],
        language: {
            url: "//cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json"
        },
        columnDefs: [
            {
                targets: 5,
                className: "desktop",
                render: function (data, type, row) {
                    let hasMatch = false;

                    for (let i = 0; i < data.postulacion_productores.length; i++) {
                        if (data.postulacion_productores[i].productor.pk === user_id) {
                            hasMatch = true;
                        }
                    }
                    console.log(hasMatch)

                    if (hasMatch) {
                        return '<button type="button" class="btn btn-success disabled">Postulando</button>';
                    } else {
                        return '<button type="button" class="btn btn-success" data-toggle="modal" data-target="#modal_postular_proceso_venta" href="#" onclick="return VerificarValidezDeProductos(' + row.pk + ',' + row.proceso_venta.solicitud_compra.pk + ');">Postular</button>';
                    }

                }
            }],
    })
}


jQuery(document).ready(function () {

    /* AGREGAR UN NUEVO PROCESO DE VENTA */
    jQuery("#form_validar_postulacion").submit(function (event) {
        event.preventDefault();
        jQuery('#modal_postular_proceso_venta').modal('hide');

        if (cumple_requisitos.every(Boolean)) {
            let formData = new FormData(jQuery(this)[0]);

            const settings = {
                url: "/api/postulacion_productor/",
                method: "POST",
                cache: false,
                contentType: false,
                processData: false,
                data: formData,
                beforeSend: function (xhr) {
                    xhr.setRequestHeader("X-CSRFToken", csrftoken);
                },
                success: function () {
                    jQuery('#lista_procesos_productor').DataTable().ajax.reload();

                    MensajeRespuesta("alert-success", "Haz postulado a esta solicitud de forma exitosa.")
                },
                failure: function () {
                    alert('Got an error');
                }
            };

            jQuery.ajax(settings).done(function (response) {
                console.log(response)
            });
        } else {
            MensajeRespuesta("alert-danger", "No cumples con los productos solicitados.")
        }
    });
});