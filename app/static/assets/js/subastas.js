function ListarSubastasFinalizadasAdmin() {
    jQuery('#lista_subastas_finalizadas').DataTable({
        ordering: true,
        info: false,
        fixedHeader: {
            header: true,
        },
        scrollCollapse: true,
        paging: false,
        ajax: "/api/subastas_finalizadas_admin/?format=datatables",
        columns: [
            {
                "data": "id_subasta",
                "searchable": true,
                width: "10%",
            },
            {
                "data": "proceso.pk",
                "searchable": true,
                width: "10%",
            },
            {
                "data": "direccion_retiro",
                "searchable": true,
            },
            {
                "data": "direccion_entrega",
                "searchable": true,
            },
            {
                "data": "estado_subasta",
                "searchable": true,
            },
            {
                data: "transportista",
                className: "text-capitalize",
                render: function (data, type, row) {
                    if (row.transportista == null) {
                        return '<p style="color:#ec5d00";>Sin asignar</p>';
                    } else {
                        return '<p style="color:#1f8a01";>' + row.transportista.user.email + '</p>\'';
                    }
                },
            }
        ],
        language: {
            url: "//cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json"
        },
        columnDefs: [{
            targets: 6,
            className: "desktop",
            render: function (data, type, row) {
                return '<button type="button" class="btn btn-success" data-toggle="modal"' +
                    ' data-target="#modal_ver_ofertas" href="#"' +
                    ' onclick="return CargarOfertas(' + row.id_subasta + ')">' +
                    'Ver ofertas</button>'
            },
        }]
    });
}

function ListarSubastasEnCursoAdmin() {
    jQuery('#lista_subastas_en_curso').DataTable({
        ordering: true,
        info: false,
        fixedHeader: {
            header: true,
        },
        scrollCollapse: true,
        paging: false,
        ajax: "/api/subastas_en_curso/?format=datatables",
        columns: [
            {
                "data": "id_subasta",
                "searchable": true,
                width: "10%",
            },
            {
                "data": "proceso.pk",
                "searchable": true,
                width: "10%",
            },
            {
                "data": "direccion_retiro",
                "searchable": true,
            },
            {
                "data": "direccion_entrega",
                "searchable": true,
            },
        ],
        language: {
            url: "//cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json"
        },
        columnDefs: [{
            targets: 4,
            className: "desktop",
            render: function (data, type, row) {
                var countDownDate = new Date(row.fecha_termino).getTime();

                // Actualizar cada 1 segundo
                var x = setInterval(function () {

                    // Obtener fecha-tiempo de hoy
                    var now = new Date();
                    now = new Date(moment(now).format('YYYY-MM-DD HH:mm:ss'));
                    now = now.getTime();

                    // Calcular la distancia
                    let distance = countDownDate - now;

                    // Calculos
                    var days = Math.floor(distance / (1000 * 60 * 60 * 24));
                    var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
                    var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
                    var seconds = Math.floor((distance % (1000 * 60)) / 1000);

                    var html = "#timerEC" + row.id_subasta.toString()

                    document.getElementById(html).innerHTML = days + "d " + hours + "h "
                        + minutes + "m " + seconds + "s ";

                    // Cuando el timer expira
                    if (distance < 0) {
                        clearInterval(x);
                        document.getElementById(html).innerHTML = "Subasta finalizada";
                        if (row.estado_subasta == "En curso") {
                            Actualizar_Subasta_Finalizada_Admin(row.id_subasta);
                        }
                    }
                });

                return '<p id="#timerEC' + row.id_subasta + '" class="fix-timer"></p> ';

            }
        }]
    });
}

function ListarSubastasAdmin() {
    jQuery('#lista_subastas_admin').DataTable({
        ordering: true,
        info: false,
        fixedHeader: {
            header: true,
        },
        scrollCollapse: true,
        paging: false,
        ajax: "/api/subastas_admin/?format=datatables",
        columns: [
            {
                "data": "id_subasta",
                "searchable": true,
                width: "10%",
            },
            {
                "data": "proceso.pk",
                "searchable": true,
                width: "10%",
            },
            {
                "data": "direccion_retiro",
                "searchable": true,
            },
            {
                "data": "direccion_entrega",
                "searchable": true,
            },
        ],
        language: {
            url: "//cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json"
        },
        columnDefs: [{
            targets: 4,
            className: "desktop",
            render: function (data, type, row) {
                var countDownDate = new Date(row.fecha_termino).getTime();

                // Actualizar cada 1 segundo
                var x = setInterval(function () {

                    // Obtener fecha-tiempo de hoy
                    var now = new Date();
                    now = new Date(moment(now).format('YYYY-MM-DD HH:mm:ss'));
                    now = now.getTime()

                    // Calcular la distancia
                    let distance = countDownDate - now;

                    // Calculos
                    var days = Math.floor(distance / (1000 * 60 * 60 * 24));
                    var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
                    var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
                    var seconds = Math.floor((distance % (1000 * 60)) / 1000);

                    var html = "#timerTS" + row.id_subasta.toString()

                    document.getElementById(html).innerHTML = days + "d " + hours + "h "
                        + minutes + "m " + seconds + "s ";

                    // Cuando el timer expira
                    if (distance < 0) {
                        clearInterval(x);
                        document.getElementById(html).innerHTML = "Subasta finalizada";
                        if (row.estado_subasta == "En curso") {
                            Actualizar_Subasta_Finalizada_Admin(row.id_subasta);
                        }
                    }
                });

                return '<p id="#timerTS' + row.id_subasta + '" class="fix-timer"></p> ';

            }
        }]
    });
}

function ListarSubastasTransportista() {
    jQuery('#lista_subastas_transportista').DataTable({
        ordering: true,
        info: false,
        fixedHeader: {
            header: true,
        },
        scrollCollapse: true,
        paging: false,
        ajax: "/api/subastas_en_curso/?format=datatables",
        columns: [
            {
                "data": "id_subasta",
                "searchable": true,
            },
            {
                "data": "direccion_retiro",
                "searchable": true,
            },
            {
                "data": "direccion_entrega",
                "searchable": true,
            },
            {
                "data": "proceso.fecha_termino",
                "searchable": true,
            },
        ],
        language: {
            url: "//cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json"
        },
        columnDefs: [{
            targets: 4,
            className: "desktop",
            render: function (data, type, row) {
                var countDownDate = new Date(row.fecha_termino).getTime();

                // Actualizar cada 1 segundo
                var x = setInterval(function () {

                    // Obtener fecha-tiempo de hoy
                    var now = new Date();
                    now = new Date(moment(now).format('YYYY-MM-DD HH:mm:ss'));
                    now = now.getTime()

                    // Calcular la distancia
                    let distance = countDownDate - now;

                    // Calculos
                    var days = Math.floor(distance / (1000 * 60 * 60 * 24));
                    var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
                    var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
                    var seconds = Math.floor((distance % (1000 * 60)) / 1000);

                    var html = "#timerST" + row.id_subasta.toString()

                    document.getElementById(html).innerHTML = days + "d " + hours + "h "
                        + minutes + "m " + seconds + "s ";

                    // Cuando el timer expira
                    if (distance < 0) {
                        clearInterval(x);
                        document.getElementById(html).innerHTML = "Subasta finalizada";
                        if (row.estado_subasta == "En curso") {
                            Actualizar_Subasta_Finalizada(row.id_subasta);
                        }
                    }
                },);

                return '<p id="#timerST' + row.id_subasta + '" class="fix-timer"></p> ';

            }
        }, {
            targets: 5,
            className: "desktop",
            render: function (data, type, row) {
                var countDownDate = new Date(row.fecha_termino);
                countDownDate = countDownDate.getTime()

                // Actualizar cada 1 segundo
                var x = setInterval(function () {

                    // Obtener fecha-tiempo de hoy
                    var now = new Date();
                    now = new Date(moment(now).format('YYYY-MM-DD HH:mm:ss'));
                    now = now.getTime()

                    // Calcular la distancia
                    let distance = countDownDate - now;

                    // Calculos
                    var days = Math.floor(distance / (1000 * 60 * 60 * 24));
                    var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
                    var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
                    var seconds = Math.floor((distance % (1000 * 60)) / 1000);

                    var html1 = "#timerbutton" + row.id_subasta.toString()

                    document.getElementById(html1).innerHTML = "Hacer oferta";

                    // Cuando el timer expira
                    if (distance < 0) {
                        clearInterval(x);
                        document.getElementById(html1).style.visibility = "hidden";
                        if (row.estado_subasta == "En curso") {
                            Actualizar_Subasta_Finalizada(row.id_subasta);
                        }
                    }
                },);


                return '<button id="#timerbutton' + row.id_subasta + '" type="button" class="btn btn-success" data-toggle="modal"' +
                    ' data-target="#modal_hacer_oferta" href="#"' +
                    ' onclick="return cargar_subasta(' + row.id_subasta + ')">' +
                    '</button>\n';

            }
        }]
    });
}


function ListarPujasTransportista() {
    jQuery('#lista_pujas_transportista').DataTable({
        ordering: true,
        info: false,
        fixedHeader: {
            header: true,
        },
        scrollCollapse: true,
        paging: false,
        ajax: "/api/pujas_transportista/?format=datatables",
        columns: [
            {
                "data": "pk",
                "searchable": true,
            },
            {
                "data": "valor_puja",
                "searchable": true,
            },
            {
                "data": "id_subasta.direccion_retiro",
                "searchable": true,
            },
            {
                "data": "id_subasta.direccion_entrega",
                "searchable": true,
            },
            {
                "data": "id_subasta.proceso.fecha_termino",
                "searchable": true,
            },
            {
                "data": "id_subasta.proceso.pk",
                "searchable": true,
            },
        ],
        language: {
            url: "//cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json"
        },
    });
}

function ListarSubastasGanadasTransportista() {
    jQuery('#lista_subastas_ganadas_transportista').DataTable({
        ordering: true,
        info: false,
        fixedHeader: {
            header: true,
        },
        scrollCollapse: true,
        paging: false,
        ajax: "/api/subastas_ganadas/?format=datatables",
        columns: [
            {
                "data": "id_subasta",
                "searchable": true,
            },
            {
                "data": "direccion_entrega",
                "searchable": true,
            },
            {
                "data": "direccion_retiro",
                "searchable": true,
            },
            {
                "data": "proceso.fecha_termino",
                "searchable": true,
            },
            {
                "data": "proceso.estado_proceso",
                "searchable": true,
            },

        ],
        language: {
            url: "//cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json"
        },
    });
}

function cargar_subasta(id_subasta) {
    jQuery.ajax({
        url: "/api/subastas_en_curso/" + id_subasta,
        type: 'GET',
        success: function (data) {
            jQuery("#id_codigo_subasta").val(data.id_subasta);
        }
    });
}

function cargar_transportista(id_transportista) {
    jQuery.ajax({
        url: "/api/transportistas/" + id_transportista,
        type: 'GET',
        success: function (data) {
            jQuery("#id_codigo_transportista").val(data.id);
        }
    });
}

function CargarOfertas(id_subasta) {
    const datatable = $('#table_ofertas_subasta').DataTable({
        ordering: true,
        info: false,
        serverSide: false,
        scrollCollapse: true,
        paging: false,
        cache: false,
        destroy: true,
        searching: false,
        ajax: '/api/pujas_admin/?format=datatables&id_subasta=' + id_subasta,
        columns: [
            {
                data: "pk",
            },
            {
                data: "id_transportista.user.first_name",
                className: "text-capitalize",
                render: function (data, type, row) {
                    return row.id_transportista.user.first_name + ' ' + row.id_transportista.user.last_name;
                },
            },
            {
                data: "valor_puja",
            }
        ],
        columnDefs: [{
            searchable: false,
            orderable: false,
            targets: 3,
            className: "desktop",
            render: function (data, type, row) {
                return '<button id="" type="button" class="btn btn-primary" data-toggle="modal"' +
                    ' data-target="#modal_contactar" href="#"' +
                    ' onclick="return CargarEmailTransportista(' + row.id_transportista.user.id + ')">' +
                    'Contactar</button>\n';
            }
        },
            {
                searchable: false,
                orderable: false,
                targets: 4,
                className: "desktop",
                render: function (data, type, row) {
                    return '<button id="" type="button" class="btn btn-success" data-toggle="modal"' +
                        ' data-target="#modal_asignar" href="#"' +
                        ' onclick="return CargarProcesoVenta(' + row.id_subasta.proceso.pk + ') ,' +
                        ' CargarTransportista(' + row.id_transportista.user.id + ') , ' +
                        'BotonAsignar(' + row.id_subasta.proceso.pk + ' , ' + row.id_transportista.user.id + ', ' + row.id_subasta.id_subasta + '),' +
                        'CargarSubasta(' + row.id_subasta.id_subasta + ') , CargarPostulacion(' + row.pk + ')">' +
                        'Asignar transporte</button>\n'
                }
            }
        ],
        order: [[1, 'asc']],
        language: {
            "emptyTable": "No hubo ninguna oferta en esta subasta.",
        },
    });

    datatable.on('order.dt search.dt', function () {
        datatable.column(0, {search: 'applied', order: 'applied'}).nodes().each(function (cell, i) {
            cell.innerHTML = i + 1;
        });
    }).draw();
}

function CargarEmailTransportista(id_transportista) {
    $.ajax({
        url: "/api/transportistas/" + id_transportista + "/",
        type: 'GET',
        success: function (data) {
            $("#email_transportista").val(data.email)
        }
    });
}


function CargarProcesoVenta(id_proceso) {
    $.ajax({
        url: "/api/procesos_ventas/" + id_proceso + "/",
        type: 'GET',
        success: function (data) {
            $("#proceso_title").text("Proceso #" + data.pk)
            $("#id_estado_proceso").val(data.estado_proceso)
            $("#id_fecha_creacion_proceso").val(data.fecha_creacion)
            $("#id_fecha_termino_proceso").val(data.fecha_termino)
        }
    })
}

function CargarTransportista(id_transportista) {
    $.ajax({
        url: "/api/transportistas/" + id_transportista + "/",
        type: 'GET',
        success: function (data) {
            $("#id_nombre_transportista").val(data.first_name + " " + data.last_name)
            $("#id_correo_transportista").val(data.email)

        }
    })
}

function CargarSubasta(id_subasta) {
    $.ajax({
        url: "/api/subastas_admin/" + id_subasta + "/",
        type: 'GET',
        success: function (data) {
            $("#subasta_title").text("Subasta #" + data.id_subasta)
            $("#id_fecha_creacion_subasta").val(data.fecha_creacion)
            $("#id_fecha_termino_subasta").val(data.fecha_termino)
            $("#id_direccion_retiro_subasta").val(data.direccion_retiro)
            $("#id_direccion_entrega_subasta").val(data.direccion_entrega)
            $("#id_estado_subasta_subasta").val(data.estado_subasta)
            $("#id_proceso_subasta").val(data.proceso.pk)
        }
    })
}

function CargarPostulacion(id_postulacion) {
    $.ajax({
        url: "/api/pujas_admin/" + id_postulacion + "/",
        type: 'GET',
        success: function (data) {
            $("#id_valor_puja_transportista").val(data.valor_puja)
        }
    })
}


function RecargarTablasTransportista() {
    jQuery('#lista_subastas_transportista').DataTable().ajax.reload();
    jQuery('#lista_pujas_transportista').DataTable().ajax.reload();
}

function RecargarTablasAdmin() {
    jQuery('#lista_subastas_finalizadas').DataTable().ajax.reload();
    jQuery('#lista_subastas_en_curso').DataTable().ajax.reload();
    jQuery('#lista_subastas_admin').DataTable().ajax.reload();
}


function BotonAsignar(id_proceso, id_transportista, id_subasta) {
    EstadoProcesoAsignar(id_proceso);
    EstadoSubastaAsignar(id_subasta, id_transportista);
    jQuery("#form_proceso_venta").submit(function (event) {
        event.preventDefault();

        let settings = {
            url: "/api/procesos_ventas/" + id_proceso + "/" + id_transportista + "/update/",
            method: "PUT",
            cache: false,
            contentType: false,
            processData: false,
            beforeSend: function (xhr) {
                xhr.setRequestHeader("X-CSRFToken", csrftoken);
            },
            failure: function (data) {
                alert('Got an error');
            }
        };

        jQuery.ajax(settings).done(function (response) {
            RecargarTablasAdmin();
            jQuery(".mensajes").append("<div class=\"row\">\n" +
                "    <div class=\"col-12\">\n" +
                "        <div id=\"success-alert\" class=\"alert alert-success alert-dismissible fade show\" role=\"alert\">\n" +
                "            <strong>Transportista " + 'asignado correctamente al proceso #' + id_proceso + ', se ha enviado un correo electrónico al transportista.' + "\n" +
                "            <button aria-label=\"Close\" class=\"close\" data-dismiss=\"alert\" type=\"button\">\n" +
                "                <span aria-hidden=\"true\">&times;</span>\n" +
                "            </button>\n" +
                "        </div>\n" +
                "    </div>\n" +
                "</div>");

            $('body').removeClass('modal-open');
            $('.modal-backdrop').remove();
            $("#cerrar_modal_ver_ofertas").click();
            $("#cerrar_asignar").click();
        });
    });
}

function EstadoSubastaAsignar(id_subasta, id_transportista) {
    console.log(id_subasta)
    jQuery("#form_proceso_venta").submit(function (event) {
        event.preventDefault();
        let settings = {
            url: "/api/subastas_admin/" + id_subasta + "/" + id_transportista + "/update/",
            method: "PUT",
            cache: false,
            contentType: false,
            processData: false,
            beforeSend: function (xhr) {
                xhr.setRequestHeader("X-CSRFToken", csrftoken);
            },
            failure: function (data) {
                alert('Got an error');
            }
        };

        jQuery.ajax(settings).done(function (response) {
        });
    });
}


function EstadoProcesoAsignar(id_proceso) {
    console.log(id_proceso)
    jQuery("#form_proceso_venta").submit(function (event) {
        event.preventDefault();
        let settings = {
            url: "/api/procesos_ventas/" + id_proceso + "/update_proceso/",
            method: "PUT",
            cache: false,
            contentType: false,
            processData: false,
            beforeSend: function (xhr) {
                xhr.setRequestHeader("X-CSRFToken", csrftoken);
            },
            failure: function (data) {
                alert('Got an error');
            }
        };

        jQuery.ajax(settings).done(function (response) {
        });
    });
}


function EstadoProcesoCrearSubasta(id_proceso) {
    let settings = {
        url: "/api/procesos_ventas/" + id_proceso + "/update_proceso_estado_subasta/",
        method: "PUT",
        cache: false,
        contentType: false,
        processData: false,
        beforeSend: function (xhr) {
            xhr.setRequestHeader("X-CSRFToken", csrftoken);
        },
        failure: function (data) {
            alert('Got an error');
        }
    };

    jQuery.ajax(settings).done(function (response) {
    });
}






