function CargarUsuario(id_usuario) {
    $.ajax({
        url: "/api/usuarios/" + id_usuario,
        type: 'GET',
        success: function (data) {
            $("#id_usuario").val(data.id)
            $("#id_nombre_usuario").val(data.first_name)
            $("#id_apellido_usuario").val(data.last_name)
            $("#id_correo_usuario").val(data.email)
            $("#id_rol_usuario").val(data.user_type)
            $("#usuario_es_activo").val(data.is_active)
            if (data.user_type === "PRODUCTOR") {
                $("#contrato_productores").show()
            } else {
                $("#contrato_productores").hide()
            }
        }
    })

    $.ajax({
        url: "/api/contrato_productor/" + id_usuario,
        type: 'GET',
        success: function (data) {
            if (data.estado === "Vigente") {
                $("#estado_contrato").addClass("badge-success")
                $("#btnActivarAnularContrato").text("Anular Contrato")
                $("#btnActivarAnularContrato").addClass("btn-danger")

                $("#btnActivarAnularContrato").click(function () {
                    if (confirm('¿Anular contrato?')) {
                        AnularContrato(data.pk)
                    }
                });

            } else {
                $("#estado_contrato").addClass("badge-danger")
                $("#btnActivarAnularContrato").text("Activar Contrato")
                $("#btnActivarAnularContrato").addClass("btn-success")

                $("#btnActivarAnularContrato").click(function () {
                    if (confirm('¿Activar contrato?')) {
                        ActivarContrato(data.pk)
                    }
                });
            }
            $("#estado_contrato").text(data.estado)
            $("#rango_contrato").text(data.fecha_inicio + " / " + data.fecha_termino)

            console.log(data)
        }
    })
}

function ActivarContrato(id_contrato) {
    jQuery.ajax({
        url: 'contrato_productor/activar/' + id_contrato + '/update/',
        type: 'PUT',
        beforeSend: function (xhr) {
            xhr.setRequestHeader("X-CSRFToken", csrftoken);
        },
        success: () => {
            location.reload();
        }
    })
}

function AnularContrato(id_contrato) {
    jQuery.ajax({
        url: 'contrato_productor/anular/' + id_contrato + '/update/',
        type: 'PUT',
        beforeSend: function (xhr) {
            xhr.setRequestHeader("X-CSRFToken", csrftoken);
        },
        success: () => {
            location.reload();
        }
    })
}

function RenderActivado(row) {
    let activar = "<button type=\"button\" class=\"btn btn-warning border-dark btn-sm\" data-toggle=\"modal\" data-target=\"#modal_editar_user\" href=\"#\" onclick=\"return CargarUsuario( " + row.id + ");\">\n" +
        "<i class=\"fa fa-lg fa-pencil\"></i>\n" +
        "</button>\n" +
        "<button type=\"button\" class=\"btn btn-success border-dark btn-sm text-dark\" data-toggle=\"modal\" data-target=\"#modal_activar_usuario\" href=\"#\" onclick=\"return cargar_activar(" + row.id + ")\">\n" +
        "<i class=\"fa fa-lg fa-unlock\"></i>\n" +
        "</button>"
    return activar
}

function RenderDesactivado(row) {

    let desactivar = "<button type=\"button\" class=\"btn btn-warning border-dark btn-sm\" data-toggle=\"modal\" data-target=\"#modal_editar_user\" href=\"#\" onclick=\"return CargarUsuario(" + row.id + ");\">\n" +
        "<i class=\"fa fa-lg fa-pencil\"></i>\n" +
        "</button>\n" +
        "<button type=\"button\" class=\"btn btn-danger border-dark btn-sm text-dark\" data-toggle=\"modal\" data-target=\"#modal_desactivar_usuario\" href=\"#\" onclick=\"return cargar_desactivar(" + row.id + ")\">\n" +
        "<i class=\"fa fa-lg fa-unlock\"></i>\n" +
        "</button>"
    return desactivar
}

function ListarProductores() {
    $('#lista_productores').DataTable({
        ordering: true,
        info: false,
        fixedHeader: {
            header: true,
        },
        scrollCollapse: true,
        paging: false,
        ajax: "/api/productores/?format=datatables",
        columns: [
            {
                data: "id",
                searchable: true,
                className: 'font-weight-bold'
            },
            {
                data: "first_name",
                searchable: true,
                render: function (data, type, row) {
                    return row.first_name + ' ' + row.last_name;
                },
                className: 'font-weight-bold'

            },
            {
                data: "user_type",
                searchable: true,
                className: 'font-weight-bold'
            },
            {
                data: null,
            },
        ],
        language: {
            url: "//cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json"
        },
        columnDefs: [{
            targets: 3,
            className: "desktop",
            render: function (data, type, row) {
                if (data.is_active === false) {
                    return RenderActivado(row);
                } else {
                    return RenderDesactivado(row);
                }
            }
        }],
        createdRow: function (row, data) {
            if (data.is_active === false) {
                $(row).addClass('bg-flat-color-4 text-capitalize');
            } else {
                $(row).addClass('bg-flat-color-5 text-capitalize');
            }
        }
    })
}

function ListarClientesExternos() {
    $('#lista_clientes_externos').DataTable({
        ordering: true,
        fixedHeader: {
            header: true,
        },
        info: false,
        scrollCollapse: true,
        paging: false,
        ajax: "/api/clientes_externos/?format=datatables",
        columns: [
            {
                data: "id",
                searchable: true,
                className: 'font-weight-bold'
            },
            {
                data: "first_name",
                searchable: true,
                render: function (data, type, row) {
                    return row.first_name + ' ' + row.last_name;
                },
                className: 'font-weight-bold'

            },
            {
                data: "user_type",
                searchable: true,
                className: 'font-weight-bold'
            },
            {
                data: null,
            },
        ],
        language: {
            url: "//cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json"
        },
        columnDefs: [{
            targets: 3,
            className: "desktop",
            render: function (data, type, row) {
                if (data.is_active === false) {
                    return RenderActivado(row);
                } else {
                    return RenderDesactivado(row);
                }
            }
        }],
        createdRow: function (row, data) {
            if (data.is_active === false) {
                $(row).addClass('bg-flat-color-4 text-capitalize');
            } else {
                $(row).addClass('bg-flat-color-5 text-capitalize');
            }
        }
    })
}

function ListarClientesInternos() {
    $('#lista_comerciantes').DataTable({
        ordering: true,
        info: false,
        fixedHeader: {
            header: true,
        },
        scrollCollapse: true,
        paging: false,
        ajax: "/api/clientes_internos/?format=datatables",
        columns: [
            {
                data: "id",
                searchable: true,
                className: 'font-weight-bold'
            },
            {
                data: "first_name",
                searchable: true,
                render: function (data, type, row) {
                    return row.first_name + ' ' + row.last_name;
                },
                className: 'font-weight-bold'

            },
            {
                data: "user_type",
                searchable: true,
                className: 'font-weight-bold'
            },
            {
                data: null,
            },
        ],
        language: {
            url: "//cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json"
        },
        columnDefs: [{
            targets: 3,
            className: "desktop",
            render: function (data, type, row) {
                if (data.is_active === false) {
                    return RenderActivado(row);
                } else {
                    return RenderDesactivado(row);
                }
            }
        }],
        createdRow: function (row, data) {
            if (data.is_active === false) {
                $(row).addClass('bg-flat-color-4 text-capitalize');
            } else {
                $(row).addClass('bg-flat-color-5 text-capitalize');
            }
        }
    })
}

function ListarTransportistas() {
    $('#lista_transportistas').DataTable({
        ordering: true,
        info: false,
        fixedHeader: {
            header: true,
        },
        scrollCollapse: true,
        paging: false,
        ajax: "/api/transportistas/?format=datatables",
        columns: [
            {
                data: "id",
                searchable: true,
                className: 'font-weight-bold'
            },
            {
                data: "first_name",
                searchable: true,
                render: function (data, type, row) {
                    return row.first_name + ' ' + row.last_name;
                },
                className: 'font-weight-bold'

            },
            {
                data: "user_type",
                searchable: true,
                className: 'font-weight-bold'
            },
            {
                data: null,
            },
        ],
        language: {
            url: "//cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json"
        },
        columnDefs: [{
            targets: 3,
            className: "desktop",
            render: function (data, type, row) {
                if (data.is_active === false) {
                    return RenderActivado(row);
                } else {
                    return RenderDesactivado(row);
                }
            }
        }],
        createdRow: function (row, data) {
            if (data.is_active === false) {
                $(row).addClass('bg-flat-color-4 text-capitalize');
            } else {
                $(row).addClass('bg-flat-color-5 text-capitalize');
            }
        }
    })
}

function ListarConsultores() {
    $('#lista_consultores').DataTable({
        ordering: true,
        info: false,
        fixedHeader: {
            header: true,
        },
        scrollCollapse: true,
        paging: false,
        ajax: "/api/consultores/?format=datatables",
        columns: [
            {
                data: "id",
                searchable: true,
                className: 'font-weight-bold'
            },
            {
                data: "first_name",
                searchable: true,
                render: function (data, type, row) {
                    return row.first_name + ' ' + row.last_name;
                },
                className: 'font-weight-bold'

            },
            {
                data: "user_type",
                searchable: true,
                className: 'font-weight-bold'
            },
            {
                data: null,
            },
        ],
        language: {
            url: "//cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json"
        },
        columnDefs: [{
            targets: 3,
            className: "desktop",
            render: function (data, type, row) {
                if (data.is_active === false) {
                    return RenderActivado(row);
                } else {
                    return RenderDesactivado(row);
                }
            }
        }],
        createdRow: function (row, data) {
            if (data.is_active === false) {
                $(row).addClass('bg-flat-color-4 text-capitalize');
            } else {
                $(row).addClass('bg-flat-color-5 text-capitalize');
            }
        }
    })
}

function ListarTodosUsuarios() {
    $('#lista_todos').DataTable({
        ordering: true,
        info: false,
        fixedHeader: {
            header: true,
        },
        scrollCollapse: true,
        paging: false,
        ajax: "/api/usuarios/?format=datatables",
        columns: [
            {
                data: "id",
                searchable: true,
                "className": 'font-weight-bold'
            },
            {
                data: "first_name",
                searchable: true,
                "render": function (data, type, row) {
                    return row.first_name + ' ' + row.last_name;
                },
                "className": 'font-weight-bold'

            },
            {
                data: "user_type",
                searchable: true,
                "className": 'font-weight-bold'
            },
            {
                data: null,
            },
        ],
        language: {
            url: "//cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json"
        },
        columnDefs: [{
            targets: 3,
            className: "desktop",
            render: function (data, type, row) {
                if (data.is_active === false) {
                    return RenderActivado(row);
                } else {
                    return RenderDesactivado(row);
                }
            }
        }],
        createdRow: function (row, data) {
            if (data.is_active === false) {
                $(row).addClass('bg-flat-color-4 text-capitalize');
            } else {
                $(row).addClass('bg-flat-color-5 text-capitalize');
            }
        }
    })
}

function RecargarTablas() {
    $('#lista_productores').DataTable().ajax.reload();
    $('#lista_clientes_externos').DataTable().ajax.reload();
    $('#lista_comerciantes').DataTable().ajax.reload();
    $('#lista_transportistas').DataTable().ajax.reload();
    $('#lista_consultores').DataTable().ajax.reload();
    $('#lista_todos').DataTable().ajax.reload();
}

function limpiarCampos() {
    $("#id_nombre_usuario").val("")
    $("#id_apellido_usuario").val("")
}

function cargar_desactivar(id_usuario) {
    $("#id_usuario_desactivar").val(id_usuario);

    $.ajax({
        url: "/api/usuarios/" + id_usuario,
        type: 'GET',
        success: function (data) {
            $("#id_tipo_usuario_desactivar").val(data.user_type);
        }
    })
}

function cargar_activar(id_usuario) {
    $("#id_usuario_activar").val(id_usuario);

    $.ajax({
        url: "/api/usuarios/" + id_usuario,
        type: 'GET',
        success: function (data) {
            $("#id_tipo_usuario_activar").val(data.user_type);
        }
    })
}

jQuery("#form_modificar_usuario").submit(function (event) {
    event.preventDefault();

    let formData = new FormData();
    formData.append('first_name', jQuery("#id_nombre_usuario").val());
    formData.append('last_name', jQuery("#id_apellido_usuario").val());
    formData.append('user_type', jQuery("#id_rol_usuario").val());
    formData.append('is_active', jQuery("#usuario_es_activo").val());


    let settings = {
        url: "/api/usuarios/" + jQuery("#id_usuario").val() + "/",
        method: "PUT",
        cache: false,
        contentType: false,
        processData: false,
        data: formData,
        beforeSend: function (xhr) {
            xhr.setRequestHeader("X-CSRFToken", csrftoken);
        },
        failure: function (data) {
            alert('Got an error');
        }
    };

    jQuery.ajax(settings).done(function (response) {
        RecargarTablas();

        let mensaje = "Usuario #" + response.id + ' ' + response.first_name + ' ' + response.last_name + " modificado con exito."
        jQuery(".mensajes").append(MensajeRespuesta("alert-success", mensaje));
        $('.mensajes').find(".alert:last").slideDown("slow");

        jQuery("#cerrar_modal_editar").click();

    });
});

jQuery("#form_desactivar_usuario").submit(function (event) {
    event.preventDefault();

    let formData = new FormData();
    formData.append('is_active', false);
    formData.append('user_type', jQuery("#id_tipo_usuario_desactivar").val());

    let settings = {
        url: "/api/usuarios/" + jQuery("#id_usuario_desactivar").val() + "/",
        method: "PUT",
        cache: false,
        contentType: false,
        processData: false,
        data: formData,
        beforeSend: function (xhr, settings) {
            xhr.setRequestHeader("X-CSRFToken", csrftoken);
        },
        failure: function (data) {
            alert('Got an error');
        }
    };

    jQuery.ajax(settings).done(function (response) {
        RecargarTablas();
        let mensaje = "Usuario #" + response.id + ' ' + response.first_name + ' ' + response.last_name + " desactivado con exito."
        jQuery(".mensajes").append(MensajeRespuesta("alert-success", mensaje));
        $('.mensajes').find(".alert:last").slideDown("slow");

        jQuery("#cerrar_modal_desactivar").click();
    });
});

jQuery("#form_activar_usuario").submit(function (event) {
    event.preventDefault();

    let formData = new FormData();
    formData.append('is_active', true);
    formData.append('user_type', jQuery("#id_tipo_usuario_activar").val());

    let settings = {
        url: "/api/usuarios/" + jQuery("#id_usuario_activar").val() + "/",
        method: "PUT",
        cache: false,
        contentType: false,
        processData: false,
        data: formData,
        beforeSend: function (xhr, settings) {
            xhr.setRequestHeader("X-CSRFToken", csrftoken);
        },
        failure: function (data) {
            alert('Got an error');
        }
    };

    jQuery.ajax(settings).done(function (response) {
        RecargarTablas();

        let mensaje = "Usuario #" + response.id + ' ' + response.first_name + ' ' + response.last_name + " activado con exito."
        jQuery(".mensajes").append(MensajeRespuesta("alert-success", mensaje));
        $('.mensajes').find(".alert:last").slideDown("slow");

        jQuery("#cerrar_modal_activar").click();
    });
});

jQuery(document).ready(function () {
    ListarProductores();
    ListarClientesExternos();
    ListarClientesInternos();
    ListarTransportistas();
    ListarConsultores();
    ListarTodosUsuarios();
});