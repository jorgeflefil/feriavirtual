let id_solicitud_compra;

function CargarNombreCliente(id_cliente) {
    $.ajax({
        url: "/api/clientes_externos/" + id_cliente + "/",
        type: 'GET',
        success: function (data) {
            $("#cliente_externo").val(data.id)
            $("#id_cliente_solicitud").val(data["first_name"] + " " + data["last_name"])
        }
    })
}


function CargarSolicitudCompra(id_solicitud, id_cliente) {
    id_solicitud_compra = id_solicitud;
    $("#id_solicitud_mostrar_modal").text("#" + id_solicitud)
    CargarNombreCliente(id_cliente)

    $.ajax({
        url: "/api/all_solicitud_de_compras/" + id_solicitud + "/",
        type: 'GET',
        success: function (data) {
            $("#id_fecha_solicitud").val(data.fecha_solicitud)
            $("#id_estado_solicitud").val(data.estado_solicitud)
            if (data.estado_solicitud == "En proceso") {
                $("#modal_detalle_solicitud_compra .modal-footer").show()
            } else {
                $("#modal_detalle_solicitud_compra .modal-footer").hide()
            }
        }
    })

    let datatable = $('#table_detalle_solicitud_de_compra').DataTable({
        ordering: true,
        info: false,
        serverSide: false,
        scrollCollapse: true,
        paging: false,
        cache: false,
        destroy: true,
        searching: false,
        ajax: '/api/producto_requerido/?format=datatables&solicitud_compra=' + id_solicitud,
        columns: [
            {
                data: null,
            },
            {
                data: "producto.nombre_producto",
                className: "text-capitalize",
            },
            {
                data: "cantidad",
                className: "text-capitalize",
            },
            {
                data: "calidad_str",
                className: "text-capitalize",
            },
        ],
        columnDefs: [{
            searchable: false,
            orderable: false,
            targets: 0
        }],
        order: [[1, 'asc']],
        language: {
            url: "//cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json"
        },
    })

    datatable.on('order.dt search.dt', function () {
        datatable.column(0, {search: 'applied', order: 'applied'}).nodes().each(function (cell, i) {
            cell.innerHTML = i + 1;
        });
    }).draw();
}

$(document).ready(function () {
    ListarProductosProductor();
});

function ListarProductosProductor() {
    $('#table_solicitudes_de_compra').DataTable({
        ordering: true,
        info: false,
        serverSide: true,
        scrollCollapse: true,
        paging: false,
        cache: false,
        ajax: '/api/all_solicitud_de_compras/?format=datatables',
        columns: [
            {
                data: "pk",
                orderable: true
            },
            {
                data: "fecha_solicitud",
                className: "text-capitalize",

            },
            {
                data: "estado_solicitud",
                render: function (data) {
                    return '<span class="align-self-center">' + data + '</span>';
                },
            },
            {
                data: "cliente_externo.pk",
            },
        ],
        language: {
            url: "//cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json"
        },

        columnDefs: [{
            targets: 3,
            className: "desktop",
            render: function (data, type, row) {
                return `<button type="button" class="btn btn-primary border-dark btn-sm" data-toggle="modal" data-target="#modal_detalle_solicitud_compra" href="#" onclick="return CargarSolicitudCompra(${row.pk},${row.cliente_externo.pk});">Ver solicitud</button>`;
            }
        }],
    })
}

$(document).ready(function () {

    /* AGREGAR UN NUEVO PROCESO DE VENTA */
    $("#form_proceso_venta").submit(function (event) {
        event.preventDefault();

        $('#modal_detalle_solicitud_compra').modal('hide');

        let formData = new FormData($(this)[0]);

        formData.append('solicitud_compra', id_solicitud_compra)
        formData.append('pedido', null)

        let settings = {
            url: "/api/procesos_ventas/",
            method: "POST",
            cache: false,
            contentType: false,
            processData: false,
            data: formData,
            beforeSend: function (xhr) {
                xhr.setRequestHeader("X-CSRFToken", csrftoken);
            },
            success: function (response) {
                let mensaje = "Proceso de venta #" + response + " aprobado con exito."
                $(".mensajes").append(MensajeRespuesta("alert-success", mensaje));
                $('.mensajes').find(".alert:last").slideDown("slow");
                $('#table_solicitudes_de_compra').DataTable().ajax.reload();
            },
            failure: function () {
                $(".mensajes").append(MensajeRespuesta("alert-danger", "Error al crear proceso de venta."));
                $('.mensajes').find(".alert:last").slideDown("slow");
            }
        };

        $.ajax(settings).done(function (response) {
            console.log(response)
        });
    });
});