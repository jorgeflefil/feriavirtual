function LoadBarChartHighQuality() {
    let productos = [];
    let dataset_high_quality = []

    $.ajax({
        url: "/api/all_products_high_quality/",
        type: 'GET',
        dataType: "json",
        success: function (data) {
            console.log(data)
            $.each(data, function (i, item) {
                console.log(item)
                productos.push(item.producto__nombre_producto);
                dataset_high_quality.push(item.cantidad__sum);
            });

            myChartHighQualityProducts.update();
        }
    })

    //bar chart
    let ctx = document.getElementById("barChartHighQualityProducts");
    //ctx.height = 150;
    let myChartHighQualityProducts = new Chart(ctx, {
        type: 'bar',
        data: {
            labels: productos,
            datasets: [
                {
                    label: "Calidad alta",
                    data: dataset_high_quality,
                    borderColor: "rgba(0, 123, 255, 0.9)",
                    borderWidth: "0",
                    backgroundColor: "rgba(0, 123, 255, 0.5)",
                },
            ]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true
                    }
                }]
            }
        }
    });
}

function LoadBarChartMediumQuality() {
    let productos = [];
    let dataset_medium_quality = []

    $.ajax({
        url: "/api/all_products_medium_quality/",
        type: 'GET',
        dataType: "json",
        success: function (data) {
            console.log(data)
            $.each(data, function (i, item) {
                console.log(item)
                productos.push(item.producto__nombre_producto);
                dataset_medium_quality.push(item.cantidad__sum);
            });
            myChartMediumQualityProducts.update();
        }
    })

    //bar chart
    let ctx = document.getElementById("barChartMediumQualityProducts");
    //ctx.height = 150;
    let myChartMediumQualityProducts = new Chart(ctx, {
        type: 'bar',
        data: {
            labels: productos,
            datasets: [
                {
                    label: "Calidad media",
                    data: dataset_medium_quality,
                    borderColor: "rgba(0, 123, 255, 0.9)",
                    borderWidth: "0",
                    backgroundColor: "rgba(0, 123, 255, 0.5)"
                },
            ]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true
                    }
                }]
            }
        }
    });
}

function LoadBarChartLowQuality() {
    let productos = [];
    let dataset_low_quality = []

    $.ajax({
        url: "/api/all_products_low_quality/",
        type: 'GET',
        dataType: "json",
        success: function (data) {
            console.log(data)
            $.each(data, function (i, item) {
                console.log(item)
                productos.push(item.producto__nombre_producto);
                dataset_low_quality.push(item.cantidad__sum);
            });
            myChartLowQualityProducts.update();
        }
    })

    //bar chart
    let ctx = document.getElementById("barChartLowQualityProducts");
    //ctx.height = 150;
    let myChartLowQualityProducts = new Chart(ctx, {
        type: 'bar',
        data: {
            labels: productos,
            datasets: [
                {
                    label: "Calidad baja",
                    data: dataset_low_quality,
                    borderColor: "rgba(0, 123, 255, 0.9)",
                    borderWidth: "0",
                    backgroundColor: "rgba(0, 123, 255, 0.5)"
                },
            ]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true
                    }
                }]
            }
        }
    });
}


$(document).ready(function () {
    LoadBarChartHighQuality()
    LoadBarChartMediumQuality()
    LoadBarChartLowQuality()
});