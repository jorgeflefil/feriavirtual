let $ = jQuery.noConflict();

let csrftoken = document.cookie.match('(^|;)\\s*' + 'csrftoken' + '\\s*=\\s*([^;]+)')
csrftoken = csrftoken ? csrftoken.pop() : ''

function MensajeRespuesta(clase, mensaje) {
    let alerta = `<div style="display: none;" class="alert ${clase} alert-dismissible" role="alert">
        <strong>${mensaje}</strong>
        <button aria-label="Close" class="close" data-dismiss="alert" type="button">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>`

    $('.mensajes').append(alerta)
    $('.mensajes').find(".alert:last").slideDown("slow");
    setTimeout(function () {
        $(".alert:last").slideUp("slow");
    }, 4000);
}

jQuery(document).ready(function ($) {

    "use strict";

    [].slice.call(document.querySelectorAll('select.cs-select')).forEach(function (el) {
        new SelectFx(el);
    });

    $('#menuToggle').on('click', function (event) {
        $('body').toggleClass('open');
    });

    $('.search-trigger').on('click', function (event) {
        event.preventDefault();
        event.stopPropagation();
        $('.search-trigger').parent('.header-left').addClass('open');
    });

    $('.search-close').on('click', function (event) {
        event.preventDefault();
        event.stopPropagation();
        $('.search-trigger').parent('.header-left').removeClass('open');
    });

    // $('.user-area> a').on('click', function(event) {
    // 	event.preventDefault();
    // 	event.stopPropagation();
    // 	$('.user-menu').parent().removeClass('open');
    // 	$('.user-menu').parent().toggleClass('open');
    // });
});

// SW
if ('serviceWorker' in navigator) {
    navigator.serviceWorker.register('/sw.js', { scope: '/' }).then(function (reg) {
        // registration worked
        console.log('Registration succeeded. Scope is ' + reg.scope);
    }).catch(function (error) {
        // registration failed
        console.log('Registration failed with ' + error);
    });
}

// Hide parent scrollbar
window.parent.document.body.style.overflow = "hidden";
