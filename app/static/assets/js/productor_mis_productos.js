let id_relacion_producto_productor;
let id_productor;
let id_producto;

function CargarSelectProducto() {
    $.ajax({
        url: "/api/lista_productos/",
        success: function (data) {
            $.each(data, function (i, val) {
                let opcion = `<option value="${val.id}">${val["nombre_producto"]}</option>`;
                $("#id_producto_productor").append(opcion)
                $(".selectpicker").selectpicker('refresh').trigger('change');

            });
        }
    })
}

function CargarModificarProductosInputSelect() {
    $.ajax({
        url: "/api/lista_productos/",
        dataType: "JSON",
        success: function (data) {
            $.each(data, function (i, val) {
                let opcion = `<option value="${val.id}">${val["nombre_producto"]}</option>`;
                $("#id_nombre_modificar_producto").append(opcion)
            });
        }
    })
}

function CargarProductoSeleccionado(id_producto) {
    $.ajax({
        url: "/api/productor_productos/" + id_producto + "/",
        type: 'GET',
        cache: false,
        success: function (data) {
            /* IMPRIMIR PRODUCTO CON/SIN IMAGEN */
            if (data.producto["imagen_producto"]) {
                $("#imagen_producto_muestra").attr("src", data.producto["imagen_producto"])
            } else {
                $("#imagen_producto_muestra").attr("src", "/media/nophoto.png")
            }

            /* GUARDAR ID DE RELACION ENTRE PRODUCTOR Y PRODUCTO */
            id_relacion_producto_productor = data.pk
            id_productor = data["productor"];
            id_producto = data["producto"].id;

            /* SELECCIONAR PRODUCTO EN SELECTPICKER */
            $('select[name=producto]').val(id_producto);
            $('#id_nombre_modificar_producto').selectpicker('refresh')
            $("#id_nombre_modificar_producto").change();

            /* CARGAR PRECIO UNITARIO Y CANTIDAD */
            $("#id_precio_unitario_modificar_producto").val(data["precio_unitario"])
            $("#id_cantidad_modificar_producto").val(data["cantidad"])

            /* CARGAR CALIDAD */
            if (data["calidad_str"] === 'Alta') {
                $("#id_calidad_modificar_producto").val(1);
            } else if (data["calidad_str"] === 'Media') {
                $("#id_calidad_modificar_producto").val(2)

            } else if (data["calidad_str"] === 'Baja') {
                $("#id_calidad_modificar_producto").val(3)
            }
        }
    })
}

function LimpiarFormularioCrearProducto() {
    $("id_nombre_nuevo_producto").val("");
    $("id_precio_unitario_nuevo_producto").val("");
    $("id_cantidad_nuevo_producto").val("");
    $("id_calidad_nuevo_producto").val("");
    $("imagen_nuevo_producto").val("");
}

function EliminarProducto(id_producto_productor) {
    $("#btn_eliminar_producto").click(function () {
        $.ajax({
            type: 'DELETE',
            url: "/api/productor_productos/" + id_producto_productor,
            beforeSend: function (xhr) {
                xhr.setRequestHeader("X-CSRFToken", csrftoken);
            },
            success: function (data) {
                $('#lista_productos_productor').DataTable().ajax.reload();

                MensajeRespuesta("alert-success", "Producto eliminado con exito.")
            },
            failure: function () {
                MensajeRespuesta("alert-danger", "Error al eliminar el producto.")
            }
        })
        $('#modal_eliminar_producto').modal('hide');

    })
}

$(document).ready(function () {
    CargarSelectProducto();
    CargarModificarProductosInputSelect();

    $('#lista_productos_productor').DataTable({
        ordering: true,
        info: false,
        serverSide: false,
        scrollCollapse: true,
        paging: false,
        cache: false,
        ajax: '/api/productor_productos/?format=datatables',
        columns: [
            {
                data: "producto.imagen_producto",
                render: function (data) {
                    if (data != null) {
                        return '<img alt="No disponible" class="img-producto" src=' + data + '/>';
                    } else {
                        return '<img alt="No disponible" class="img-producto" src="/media/nophoto.png" />';
                    }
                },
                width: "10%",
                orderable: false
            },
            {
                data: "producto.nombre_producto",
                className: "text-capitalize",
                orderable: true
            },
            {
                data: "precio_unitario",
                width: "15%",
                render: function (data) {
                    return '<span class="align-self-center">$' + data + '</span>';
                },
            },
            {
                width: "15%",
                data: "cantidad",
            },
            {
                data: "calidad_str",
            },
            {
                data: null
            },
        ],
        language: {
            url: "//cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json"
        },

        columnDefs: [{
            targets: 5,
            className: "desktop",
            render: function (data, type, row) {
                return '<button type="button" class="btn btn-warning border-dark btn-sm text-secondary" data-toggle="modal" data-target="#modal_modificar_producto" href="#" onclick="return CargarProductoSeleccionado(' + data["pk"] + ');">' +
                    '<i class="fa fa-lg fa-pencil" style="color: black"></i>\n' +
                    '</button>\n' +
                    '<button type="button" class="btn btn-danger border-dark btn-sm text-secondary" data-toggle="modal" data-target="#modal_eliminar_producto" href="#" onclick="return EliminarProducto(' + data["pk"] + ')">' +
                    '<i class="fa fa-lg fa-times" style="color: black"></i>\n' +
                    '</button>';
            }
        }],
        createdRow: function (row, data) {
            if (data.cantidad == 0) {
                $(row).addClass('bg-flat-color-4 text-capitalize');
            }
            //else {$(row).addClass('bg-flat-color-5 text-capitalize');}
        }
    })

    /* AGREGAR UN NUEVO PRODUCTO */
    $("#form_agregar_nuevo_producto").submit(function (event) {
        event.preventDefault();

        $('#modal_crear_producto').modal('hide');

        let formData = new FormData($(this)[0]);
        formData.append('producto', $("#id_producto_productor").val())
        formData.append('calidad', $("#id_calidad_nuevo_producto").val())

        const settings = {
            url: "/api/productor_productos/",
            method: "POST",
            cache: false,
            contentType: false,
            processData: false,
            data: formData,
            beforeSend: function (xhr) {
                xhr.setRequestHeader("X-CSRFToken", csrftoken);
            },
            success: function () {
                $('#lista_productos_productor').DataTable().ajax.reload();
                //$("#id_producto_productor").val('default');
                $("#id_producto_productor").selectpicker('val', 'default');
                $("#id_producto_productor").selectpicker("refresh");

                $('#id_precio_unitario_nuevo_producto').val("");
                $('#id_calidad_nuevo_producto').val("");
                $('#id_cantidad_nuevo_producto').val("");

                MensajeRespuesta("alert-success", "Producto creado con exito.")
            },
            failure: function () {
                MensajeRespuesta("alert-danger", "Error al crear producto.")
            }
        };

        $.ajax(settings).done(function (response) {
            LimpiarFormularioCrearProducto();
        });
    });

    /* MODIFICAR UN PRODUCTO */
    $("#form_modificar_producto").submit(function (event) {
        event.preventDefault();

        $('#modal_modificar_producto').modal('hide');

        let formData = new FormData($(this)[0]);

        formData.append('id_relacion_producto_productor', id_relacion_producto_productor)
        formData.append('productor', id_productor)
        formData.append('calidad', $("#id_calidad_modificar_producto").val())


        const settings = {
            url: "/api/productor/mis_productos/" + id_relacion_producto_productor + "/update/",
            method: "PUT",
            cache: false,
            contentType: false,
            processData: false,
            data: formData,
            beforeSend: function (xhr) {
                xhr.setRequestHeader("X-CSRFToken", csrftoken);
            },
            success: function () {
                $('#lista_productos_productor').DataTable().ajax.reload();

                MensajeRespuesta("alert-success", "Producto modificado con exito.")
            },
            failure: function () {
                MensajeRespuesta("alert-danger", "Error al modificar producto.")
            }
        };

        $.ajax(settings).done(function (response) {
            LimpiarFormularioCrearProducto();
        });
    });
});