from django.contrib import admin

from .models import *

# Register your models here.
admin.site.register(Producto)
admin.site.register(User)
admin.site.register(ContratoProductor)
admin.site.register(Productor)
admin.site.register(ClienteExterno)
admin.site.register(ClienteInterno)
admin.site.register(Transportista)
admin.site.register(Consultor)
admin.site.register(Cart)
admin.site.register(ProductorProducto)
admin.site.register(SolicitudDeCompra)
admin.site.register(Pedido)
admin.site.register(ProductoRequerido)
admin.site.register(Subasta)
admin.site.register(PostulacionTransportista)
admin.site.register(ProcesoVenta)
admin.site.register(ProcesoPostulacion)
admin.site.register(PostulacionProductor)
admin.site.register(EnvioProductoProductor)
