from datetime import datetime, timedelta

from django.contrib.auth import get_user_model
from django.contrib.auth.models import AbstractUser
from django.contrib.auth.models import User
from django.db import models
from image_optimizer.fields import OptimizedImageField

calidad_alta = 1
calidad_media = 2
calidad_baja = 3

OPCIONES_CALIDAD = (
    (calidad_alta, 'Alta'),
    (calidad_media, 'Media'),
    (calidad_baja, 'Baja'),
)


class Producto(models.Model):
    imagen_producto = OptimizedImageField(upload_to='', blank=True)
    nombre_producto = models.CharField(max_length=50)


class User(AbstractUser):
    cliente_externo = 1
    cliente_interno = 2
    transportista = 3
    consultor = 4
    productor = 5

    USER_TYPE_CHOICES = (
        (cliente_externo, 'CLIENTE EXTERNO'),
        (cliente_interno, 'CLIENTE INTERNO'),
        (transportista, 'TRANSPORTISTA'),
        (consultor, 'CONSULTOR'),
        (productor, 'PRODUCTOR'),
    )

    OPCIONES_ADMINISTRADOR = (
        (consultor, 'Consultor'),
        (transportista, 'Transportista'),
    )

    OPCIONES_SIGN_UP = (
        (cliente_interno, 'Soy minorista'),
        (cliente_externo, 'Soy mayorista'),
    )

    PRODUCTOR_SING_UP = (
        (productor, 'Soy productor'),
    )

    TRANSPORTISTA_SING_UP = (
        (transportista, 'Soy transportista'),
    )

    ADMIN_REGISTRAR = (
        (transportista, "Transportista"),
        (consultor, "Consultor")
    )

    user_type = models.PositiveSmallIntegerField(choices=USER_TYPE_CHOICES, null=True)


User = get_user_model()


class Activation(models.Model):
    User = models.ForeignKey(User, on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)
    code = models.CharField(max_length=20, unique=True)
    email = models.EmailField(blank=True)


class Productor(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, primary_key=True)

    class Meta:
        verbose_name_plural = "Productores"


class ContratoProductor(models.Model):
    productor = models.OneToOneField(Productor, on_delete=models.PROTECT, primary_key=True)
    fecha_inicio = models.DateField(blank=True)
    fecha_termino = models.DateField(blank=True)
    estado = models.CharField(max_length=50)


class ClienteExterno(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, primary_key=True)


class ClienteInterno(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, primary_key=True)


class Transportista(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, primary_key=True)


class Consultor(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, primary_key=True)


class ProductorProducto(models.Model):
    productor = models.ForeignKey(Productor, on_delete=models.PROTECT)
    producto = models.ForeignKey(Producto, on_delete=models.PROTECT)
    precio_unitario = models.PositiveSmallIntegerField(default=1)
    cantidad = models.PositiveSmallIntegerField(default=1)
    calidad = models.PositiveSmallIntegerField(choices=OPCIONES_CALIDAD, null=True)
    interna = models.BooleanField(default=False)
    unidad = models.CharField(max_length=50, default='KG')


class SolicitudDeCompra(models.Model):
    cliente_externo = models.ForeignKey(ClienteExterno, on_delete=models.PROTECT)
    fecha_solicitud = models.DateField(auto_now=True)
    fecha_solicitud = models.DateField(auto_now=True)
    estado_solicitud = models.CharField(max_length=50)


class Pedido(models.Model):
    cliente_interno = models.ForeignKey(ClienteInterno, on_delete=models.PROTECT)
    fecha_solicitud = models.DateField(auto_now=True)
    estado_pedido = models.CharField(max_length=50)


class ProductoRequerido(models.Model):
    solicitud_compra = models.ForeignKey(SolicitudDeCompra, on_delete=models.CASCADE, related_name='productos_requeridos')
    producto = models.ForeignKey(Producto, on_delete=models.PROTECT)
    cantidad = models.PositiveSmallIntegerField(default=1)
    calidad = models.PositiveSmallIntegerField(choices=OPCIONES_CALIDAD, null=True)


def get_fecha_termino_proceso_venta():
    return datetime.today() + timedelta(days=30)


class ProcesoVenta(models.Model):
    estado_proceso = models.CharField(max_length=50)
    fecha_creacion = models.DateField(auto_now=True)
    fecha_termino = models.DateField(default=get_fecha_termino_proceso_venta)
    solicitud_compra = models.ForeignKey(SolicitudDeCompra, on_delete=models.PROTECT, blank=True, null=True)
    pedido = models.ForeignKey(Pedido, on_delete=models.PROTECT, blank=True, null=True)
    transportista = models.ForeignKey(Transportista, on_delete=models.PROTECT, blank=True, null=True)


def get_fecha_termino_proceso_postulacion():
    #return datetime.today() + timedelta(days=7)
    return datetime.today()


class ProcesoPostulacion(models.Model):
    proceso_venta = models.OneToOneField(ProcesoVenta, on_delete=models.CASCADE, primary_key=True)
    fecha_inicio_postulacion = models.DateField(auto_now=True)
    fecha_fin_postulacion = models.DateField(default=get_fecha_termino_proceso_postulacion)
    estado_proceso = models.CharField(max_length=50)


class PostulacionProductor(models.Model):
    proceso_postulacion = models.ForeignKey(ProcesoPostulacion, on_delete=models.PROTECT)
    productor = models.ForeignKey(Productor, on_delete=models.PROTECT)
    estado_postulacion = models.CharField(max_length=50, default='Postulando')


class Subasta(models.Model):
    id_subasta = models.AutoField(primary_key=True)
    fecha_creacion = models.DateTimeField(auto_now_add=True)
    fecha_termino = models.DateTimeField(null=True, blank=True)
    direccion_retiro = models.CharField(max_length=120)
    direccion_entrega = models.CharField(max_length=120)
    estado_subasta = models.CharField(max_length=50)
    proceso = models.ForeignKey(ProcesoVenta, on_delete=models.PROTECT)
    transportista = models.ForeignKey(Transportista, on_delete=models.PROTECT, blank=True, null=True)


class PostulacionTransportista(models.Model):
    id_subasta = models.ForeignKey(Subasta, on_delete=models.PROTECT)
    id_transportista = models.ForeignKey(Transportista, on_delete=models.PROTECT)
    valor_puja = models.PositiveSmallIntegerField(default=1)
    estado_postulacion = models.CharField(max_length=50)


class EnvioProductoProductor(models.Model):
    postulacion_productor = models.ForeignKey(PostulacionProductor, on_delete=models.PROTECT)
    productor = models.ForeignKey(Productor, on_delete=models.PROTECT)
    producto = models.ForeignKey(Producto, on_delete=models.PROTECT)
    precio_unitario = models.PositiveSmallIntegerField(default=1)
    cantidad = models.PositiveSmallIntegerField(default=1)
    calidad = models.PositiveSmallIntegerField(choices=OPCIONES_CALIDAD)


class UserAddress(models.Model):
    address_id = models.AutoField(primary_key=True)
    user_id = models.ForeignKey(User, on_delete=models.PROTECT)
    street = models.CharField(max_length=50)
    number = models.CharField(max_length=50)
    region = models.CharField(max_length=50)
    comuna = models.CharField(max_length=50)
    enabled = models.BooleanField(default=True)


class Payment(models.Model):
    payment_id = models.AutoField(primary_key=True)
    description = models.CharField(max_length=50)


class Order(models.Model):
    order_id = models.AutoField(primary_key=True)
    user_id = models.ForeignKey(User, on_delete=models.PROTECT)
    address_id = models.ForeignKey(UserAddress, on_delete=models.PROTECT)
    payment_id = models.ForeignKey(Payment, on_delete=models.PROTECT)
    status = models.CharField(max_length=50, default='Unpaid')
    date = models.DateTimeField(auto_now_add=True)


class Cart(models.Model):
    user_id = models.ForeignKey(User, on_delete=models.PROTECT)
    product_id = models.ForeignKey(Producto, on_delete=models.PROTECT)
    productor_id = models.ForeignKey(ProductorProducto, on_delete=models.PROTECT)
    order_id = models.ForeignKey(Order, on_delete=models.PROTECT)
    quantity = models.PositiveIntegerField(default=1, null=False)
    price = models.PositiveIntegerField(default=1)
    unidad = models.CharField(max_length=50, default='KG')
