from django.conf import settings
from django.contrib.staticfiles.urls import static
from django.urls import path, include
from django.views.generic import TemplateView
from rest_framework import routers

from .views import custom_auth
from .views import views
from .views.views import venta_externa_render_pdf_view

router = routers.DefaultRouter()

router.register(r'productos', views.ProductoViewSet, basename='productos')
router.register(r'lista_productos', views.ListaProductoViewSet, basename='lista_productos')
router.register(r'all_products', views.GetAllProducts, basename='all_products')
router.register(r'all_products_high_quality', views.GetAllProductsHighQuality, basename='all_products_high_quality')
router.register(r'all_products_medium_quality', views.GetAllProductsMediumQuality,
                basename='all_products_medium_quality')
router.register(r'all_products_low_quality', views.GetAllProductsLowQuality, basename='all_products_low_quality')

router.register(r'shopping', views.Shopping, basename='shopping')
router.register(r'postular_procesos', views.ProcesoProductorViewSet)
router.register(r'postulacion_productor', views.PostulacionProductorViewSet)

# Productor
router.register(r'mis_postulaciones_productor', views.MisPostulacionesProductorViewSet,
                basename='mis_postulaciones_productor')
router.register(r'proceso_postulacion_productor', views.ProcesoPostulacionProductorViewSet, basename='proceso_postulacion_productor')
router.register(r'detalle_proceso_postulacion_productor', views.DetalleProcesoPostulacionProductorViewSet,
                basename='detalle_proceso_postulacion_productor')
router.register(r'productor_productos', views.UserProductorProductoViewSet, basename='productor_productos')
router.register(r'contrato_productor', views.GetAllContratoProductor, basename='contrato_productor')

# ADMIN
router.register(r'procesos_admin', views.ProcesoAdminViewSet)
router.register(r'detalle_procesos_admin', views.DetalleProcesoPostulacionAdminViewSet,
                basename='detalle_procesos_admin')
router.register(r'procesos_ventas', views.ProcesoVentaAdminViewSet)
router.register(r'subastas_admin', views.SubastasAdminViewSet, basename='subastas_admin')
router.register(r'subastas_finalizadas_admin', views.SubastasFinalizadasViewSet, basename='subastas_finalizadas_admin')
router.register(r'procesos_ventas', views.ProcesoVentaAdminViewSet)
router.register(r'pujas_admin', views.PujasAdminViewSet, basename='pujas_admin')

# TRANSPORTISTA
router.register(r'subastas_en_curso', views.SubastasTransportistaViewSet, basename='subastas_en_curso')
router.register(r'subastas_ganadas', views.SubastasGanadasTransportistaViewSet, basename='subastas_ganadas')
router.register(r'pujas_transportista', views.PujasTransportistaViewSet, basename='pujas_transportista')
router.register(r'pujas', views.PujasViewSet, basename='pujas')
router.register(r'mejor_oferta', views.MejorOfertaTransportistaViewSet, basename='mejor_oferta')

# CLIENTE EXTERNO
router.register(r'mis_procesos_venta_cliente_externo', views.ClienteExternoMisProcesosVentaProductorViewSet,
                basename='mis_procesos_venta_cliente_externo')
router.register(r'all_solicitud_de_compras', views.AllSolicitudDeCompraViewSet, basename='all_solicitud_de_compras')
router.register(r'solicitud_de_compras', views.SolicitudDeCompraViewSet, basename='solicitud_de_compras')
router.register(r'detalle_solicitud_de_compra', views.DetalleSolicitudDeCompraViewSet,
                basename='detalle_solicitud_de_compras')
router.register(r'producto_requerido', views.ProductoRequeridoViewSet, basename='producto_requerido')
router.register(r'productor_producto_requerido', views.ProductorValidarProductoRequeridoViewSet)

# VIEWSETS USUARIOS
router.register(r'usuarios', views.UserViewSet, basename='usuarios')
router.register(r'productores', views.UserProductorViewSet, basename='productores')
router.register(r'clientes_externos', views.UserClienteExternoViewSet, basename='clientes_externos')
router.register(r'clientes_internos', views.UserClienteInternoViewSet, basename='clientes_internos')
router.register(r'consultores', views.UserConsultorViewSet, basename='consultores')
router.register(r'transportistas', views.UserTransportistaViewSet, basename='transportistas')

urlpatterns = [
    path('', views.content, name='index'),
    path('frame/', views.index, name='main'),

    # VENTA
    path('venta/interna/', views.content, name='venta_interna'),
    path('products/', views.content, name='products'),
    path('cart/', views.content, name='cart'),

    # CLIENTE EXTERNO
    path('cliente/solicitud_de_compra/', views.content, name='mis_solicitudes_de_compra'),
    path('cliente_externo/mis_procesos_venta/', views.content, name='cliente_externo_mis_procesos_venta'),

    # PRODUCTOR FRAME
    path('postular/', views.content, name='postular'),
    path('mis_productos/', views.content, name='mis_productos'),
    path('productor/mis_procesos_venta/', views.content, name='productor_mis_procesos_venta'),

    # DJANGO REST FRAMEWORK
    path('productor/mis_productos/', views.UserProductorProductoViewSet, name='productor_productos'),
    path('api/productor/mis_productos/<id_relacion>/update/', views.api_actualizar_productor_producto,
         name='update_productor_producto'),
    path('api/cliente_externo/solicitud_de_compra/create/', views.api_crear_solicitud_compra,
         name='create_solicitud_compra'),
    path('venta/interna/<id_relacion>/update/', views.update_interna, name='update_interna'),
    path('venta/interna/unidad/<id_relacion>/update/', views.update_interna_unidad, name='update_interna_unidad'),
    path('cart/addtocart/', views.add_to_cart, name='add_to_cart'),
    path('cart/removefromcart/<id>', views.removecart, name='remove_cart'),
    path('address', views.address, name='address'),
    path('new_address', views.new_address, name='new_address'),
    path('delete_address', views.delete_address, name='delete_address'),
    path('update_order_address', views.update_order_address, name='update_order_address'),
    path('update_order_payment', views.update_order_payment, name='update_order_payment'),
    path('update_order_status', views.update_order_status, name='update_order_status'),
    path('get_product_price/', views.get_product_price, name='get_product_price'),
    path('productor/mis_productos/', views.UserProductorProductoViewSet, name='productor_productos'),
    path('contrato_productor/activar/<id_contrato>/update/', views.update_contrato_activar,
         name='update_contrato_activar'),
    path('contrato_productor/anular/<id_contrato>/update/', views.update_contrato_anular,
         name='update_contrato_anular'),
    path('api/procesos_ventas/<id_proceso>/<transportista>/update/', views.update_proceso_transportista,
         name='update_proceso_transportista'),
    path('api/subastas_admin/<id_subasta>/<transportista>/update/', views.update_subasta_asignar,
         name='update_subasta_asignar'),
    path('api/subastas_admin/<id_subasta>/update_finalizar/', views.update_subasta_estado,
         name='update_subasta_estado'),
    path('api/procesos_ventas/<id_proceso>/update_proceso/', views.update_proceso, name='update_proceso'),
    path('api/procesos_ventas/<id_proceso>/update_proceso_estado_subasta/',
         views.update_proceso_estado_subasta, name='update_proceso_estado_subasta'),
    path('api/procesos_ventas/<id_proceso>/update_recibir/',
        views.update_recibir, name='update_recibir'),

    # TRANSPORTISTA
    path('subastas_transportista/', views.content, name='subastas_transportista'),
    path('subastas_ganadas_transportista/', views.content, name='subastas_ganadas_transportista'),

    #    # CHECKOUT
    path('checkout/shipping/', views.content, name="shipping"),
    path('checkout/payment/', views.content, name="payment"),
    path('checkout/gateway/', views.content, name="gateway"),
    path('checkout/confirmation/', views.content, name="confirmation"),
    path('my_address/', views.content, name="my_address"),
    path('my_orders/', views.content, name="my_orders"),
    path('my_orders_detail/', views.content, name="my_orders_detail"),
    path('reject/', views.reject, name="reject"),

    # ADMIN
    path('admin/procesos_admin/', views.content, name='procesos_admin'),
    path('admin/mantenedor/usuarios/', views.content, name='mantenedor'),
    path('admin/mantenedor/solicitudes_compra/', views.content, name='mantenedor_solicitudes_compra'),
    path('admin/subastas/crear_subasta/', views.content, name='crear_subasta'),
    path('admin/subastas/contactar/', views.content, name='contactar'),
    path('admin/reportes/', views.content, name='reportes_admin'),

    path('log-in/', views.content, name='log_in'),
    path('log-out/', views.content, name='log_out'),

    path('resend/activation-code/', views.content, name='resend_activation_code'),

    path('registro/', views.content, name='sign_up'),
    path('registro_productor/', views.content, name='sign_up_prod'),
    path('registro_transportista/', views.content, name='sign_up_transportista'),
    path('admin/mantenedor/', views.content, name='sign_up_admin'),
    path('activate/<code>/', custom_auth.ActivateView.as_view(), name='activate'),

    path('restore/password/', views.content, name='restore_password'),
    path('restore/password/done/', views.content, name='restore_password_done'),
    path('restore/<uidb64>/<token>/', views.content, name='restore_password_confirm'),

    path('remind/username/', views.content, name='remind_username'),

    path('sw.js', TemplateView.as_view(template_name='sw.js', content_type='application/javascript'),
         name='sw.js', ),

    path('change/profile/', views.content, name='change_profile'),
    path('change/password/', views.content, name='change_password'),
    path('change/email/', views.content, name='change_email'),
    path('change/email/<code>/', custom_auth.ChangeEmailActivateView.as_view(), name='change_email_activation'),
    path('api/', include(router.urls)),
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework')),

    ####################### FRAME URLS #######################

    # VENTA
    path('venta/interna/frame', views.venta_interna),
    path('products/frame', views.product_list),
    path('cart/frame', views.basket),

    # CLIENTE EXTERNO
    path('cliente/solicitud_de_compra/frame', views.mis_solicitudes_de_compra),
    path('cliente_externo/mis_procesos_venta/frame', views.cliente_externo_mis_procesos_venta),

    # PRODUCTOR FRAME
    path('postular/frame', views.ProcesoProductorListView.as_view()),
    path('mis_productos/frame', views.mis_productos),
    path('productor/mis_procesos_venta/frame', views.productor_mis_procesos_venta),

    # TRANSPORTISTA
    path('subastas_transportista/frame', views.SubastasListView.as_view()),
    path('subastas_ganadas_transportista/frame', views.SubastasGanadasListView.as_view()),

    # CHECKOUT
    path('checkout/shipping/frame', views.shipping),
    path('checkout/payment/frame', views.payment),
    path('checkout/gateway/frame', views.gateway),
    path('checkout/confirmation/frame', views.confirmation),
    path('my_address/frame', views.my_address),
    path('my_orders/frame', views.my_orders),
    path('my_orders_detail/frame', views.my_orders_detail),

    # ADMIN
    path('admin/procesos_admin/frame', views.ProcesoAdminListView.as_view()),
    path('admin/mantenedor/usuarios/frame', custom_auth.SignUpViewAdmin.as_view()),
    path('admin/mantenedor/solicitudes_compra/frame', views.mantenedor_solicitudes_compra),
    path('admin/subastas/crear_subasta/frame', views.CrearSubastaView.as_view(), name="crear_subasta/frame"),
    path('admin/reportes/frame', views.reportes_admin),
    path('admin/subastas/contactar/frame', views.contactar_transportista, name="contactar/frame"),
    path('admin/reportes/frame', views.reportes_admin),

    path('log-in/frame', custom_auth.LogInView.as_view(), name="log_in/frame"),
    path('log-out/frame', custom_auth.LogOutView.as_view(), name="log-out/frame"),

    path('resend/activation-code/frame', custom_auth.ResendActivationCodeView.as_view()),

    path('registro/frame', custom_auth.SignUpView.as_view(), name="sign_up/frame"),
    path('registro_productor/frame', custom_auth.RegisterProd.as_view(), name="sign_up_prod/frame"),
    path('registro_transportista/frame', custom_auth.RegisterTransportista.as_view(), name="sign_up_transportista/frame"),
    path('admin/mantenedor/frame', custom_auth.SignUpViewAdmin.as_view(), name="sign_up_admin/frame"),

    path('restore/password/frame', custom_auth.RestorePasswordView.as_view()),
    path('restore/password/done/frame', custom_auth.RestorePasswordDoneView.as_view()),
    path('restore/<uidb64>/<token>/frame', custom_auth.RestorePasswordConfirmView.as_view()),

    path('remind/username/frame', custom_auth.RemindUsernameView.as_view()),

    path('change/profile/frame', custom_auth.ChangeProfileView.as_view()),
    path('change/password/frame', custom_auth.ChangePasswordView.as_view()),
    path('change/email/frame', custom_auth.ChangeEmailView.as_view()),

    path('pdf_proceso_venta/<pk>', venta_externa_render_pdf_view, name='venta_externa_pdf_view'),

]  # + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
